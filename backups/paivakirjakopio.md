



## Webdev kurssin päiväkirja

### Lähtötaso kurssin alusta
- HTML taidoti ovat hyvät, pystyn tuottamaan verkkosivuja. Ymmärrän erilaiset elementit ja kuinka käyttää niitä.
- CSS taidot ovat kehittyneet HTML taitojeni kanssa, joten sivujen tyylitys onnistuu vaivatta.
- Javascript taitoni ovat hyvät, pystyn muokkaamaan verkkosivua DOMia käyttäen. Osaan hyödyntää JQuery kirjastoa ja muita vastaavia.
- Nodejs, osaan hyödyntää Nodejs:än frameworkeja/ kirjastoja esim. Express:iä ja mysql.
- SQL, ymmärrän kuinka tietokannat toimivat ja kuinka niitä käytetään. (Tietuieden vienti ja hakeminen tietokannasta)



#### 7.9.
Ensimmäisen viikkotehtävän kanssa ei ollut ongelmia, kaikki vaaditut osat tuli
täytettyä tehtävänannosta. Nodejs on entuudestaan tuttu ja pystyn tuottamaan koodia vaivatta.


#### 12.9.
Toisen viikon hommia aloitin ja pääsin alkuun toteutuksessa. Tehtävän loppuun tekeminen on nopeasti tehty, kunnes olen saanut tehtyä peli puolen tehtävät. Tehtävässä ei ilmennyt ongelmia sen aloittamisen yhteydessä, eikä myöskään tehtävän edetessä.

GitLabiin repon luonti ei tuottanut ongelmia.









