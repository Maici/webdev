




class Bar
{
    constructor(x)
    {
        this._x = x;
    }

    get x() {
        return this._x;
    }
    set x(x) {
        this._x = x;
    }

}



bar = new Bar(123);

console.log( bar.x );

bar.x = 34;

console.log( bar.x );

