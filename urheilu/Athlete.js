

// Athlete.js

let Person = require('./Person');

class Athlete extends Person
{
    constructor(firstname, lastname, nickname, birthyear, profile_img, weight, sport)
    {
        super(firstname, lastname, nickname, birthyear);
        this.m_profile_img = profile_img;
        this.m_weight = weight;
        this.m_sport = sport;
        this.m_achievements = [];


    }

}

// Getters
Athlete.prototype.getProfileImg = function()
{
    return this.m_profile_img
}
Athlete.prototype.getWeight = function()
{
    return this.m_weight;
}
Athlete.prototype.getSport = function()
{
    return this.m_sport;
}
Athlete.prototype.getAchievements = function()
{
    return this.m_achievements;
}

// Setters
Athlete.prototype.setProfileImg = function(profile_img)
{
    this.m_profile_img = profile_img;
}
Athlete.prototype.setWeight = function(weight)
{
    this.m_weight = weight;
}
Athlete.prototype.setSport = function(sport)
{
    this.m_sport = sport;
}

// Add
Athlete.prototype.addAchievement = function(achievement)
{
    this.m_achievements.push(achievement);
}



module.exports = Athlete;