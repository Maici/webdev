

let Person = require('./Person');
let Athlete = require('./Athlete');


let athletes = [];


first_names = ["Adam", "Alex", "Aaron", "Ben", "Carl", "Dan", "David", "Edward", "Fred", "Frank", "George", "Hal", "Hank", "Ike", "John"];
last_names = ["Anderson", "Ashwoon", "Aikin", "Bateman", "Bongard", "Bowers", "Boyd", "Cannon", "Cast", "Deitz", "Dewalt", "Ebner", "Frick"];
sports = ['Boxing','Canoe Slalom','Cycling Mountain Bike','Cycling Road','Cycling Track','Diving','Fencing','Football'];


// Create random athletes
function makeRandomAthletes()
{

    for (i = 0; i < 10; i++)
    {
        first_name1 = first_names[parseInt(Math.random() * first_names.length - 1)];
        first_name2 = first_names[parseInt(Math.random() * first_names.length - 1)];
        last_name = last_names[parseInt(Math.random() * last_names.length - 1)];
        sport = sports[parseInt(Math.random() * sports.length - 1)];

        birth_year = parseInt(2000 - Math.random() * 30);
        weight = parseInt((100 - Math.random() * 40) * 100) / 100;

        athletes.push( new Athlete([first_name1, first_name2], last_name, first_name1, birth_year, '', weight, sport) );

    }

}

makeRandomAthletes();


console.log( athletes );









