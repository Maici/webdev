

// Person.js

class Person
{
    constructor(first_name, last_name, nick_name, birth_year)
    {
        this.m_first_names = [first_name[0], first_name[1]];
        this.m_last_name = last_name;
        this.m_nick_name = nick_name;
        this.m_birth_year = birth_year;
    }

}


// Getters
Person.prototype.getFirstNames = function()
{
    return this.m_first_names;
}
Person.prototype.getLastName = function()
{
    return this.m_last_name;
}
Person.prototype.getNickName = function()
{
    return this.m_nick_name;
}
Person.prototype.getBirthYear = function()
{
    return this.m_birth_year;
}


// Setters
Person.prototype.setFirstNames = function(first_name)
{
    this.m_first_names = first_name;
}
Person.prototype.setLastName = function(last_name)
{
    this.m_last_name = last_name;
}
Person.prototype.setNickName = function(nick_name)
{
    this.m_nick_name = nick_name;
}
Person.prototype.setBirthYear = function(birth_year)
{
    this.m_birth_year = birth_year;
}



module.exports = Person;

