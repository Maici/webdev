

### Linkki päiväkirjaan
- https://hackmd.io/PlSMdkoQRGeV77dz71q2CQ?edit



### Tehtävät

#### Valmis
- Ensimmäinen palautettava tehtävä (urheilu) löytyy gitlabista.

- Toisen viikon REST hommelit (löytyvät tiedostosta server.js)
    - Karkea GUI, joka toimii. Pitäisi toimia.
    - Palvelimelta tuleva vastaus tulee selaimen consoli logiin.



#### Tekovaiheessa




#### Huomioitavaa (lähinnä itselleni)
- Set:in ja Get:in käyttäminen urheilu luokissa

- Poikkeuksien lisäileminen tarkemmin

- Osa koodista tarvitsee hiomista

