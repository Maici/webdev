



const   axios = require('axios')

let sensors;

url = 'http://localhost:3000/records';


axios.get(url, {
    params: {
        type: 'sensor'
    }
})
.then((res) => {
    console.log(res.data.resultSet[0].Sensor_ID);

    sensors = res.data;

    GeneStuff();
})


function GeneStuff() {
    // url palvelimelle
    url = 'http://localhost:3000/sensor/'+sensors.resultSet[0].Sensor_ID;

    // nykyinen päivä vuodesta
    currentDay = 0;

    // sensorin aloitus arvo
    sensorValue = 25;

    // päivitys tiheys arvo esim. 1000ms / 10 = 100ms
    tickTime = 1000;

    // Toista koodia ticktime välein

    setInterval(() => {

        // kasvata päivää yhdellä mikäli alle 365
        // palauta päivämäärä 0 jos ei alle 365
        if (currentDay < 365) {
            currentDay++;
        } else {
            currentDay = 0;
        }

        // lisätään sensorin arvoon random value
        sensorValue += Math.random() * 1 - 0.5;

        // kaunistellaan sensorin arvoa kahden desimaalin tarkkuudelle
        sensorValue = Math.round(sensorValue * 100) / 100;

        // alkeellinen testimielessä toteutettu vuoden ajan simulointi
        // kaipaa huomattavasti enemmän ajatusta taakse
        if (currentDay > 250) {
            realValue = sensorValue + 5;
        } else if (currentDay > 125) {
            realValue = sensorValue + 1;
        } else {
            realValue = sensorValue - 5;
        }

        // samaa pyöristystä
        realValue = Math.round(realValue * 100) / 100;

        // lähetä sensori dataa palvelimelle
        axios.post(url, {
            value: realValue
        })
        .then((res) => {
            console.log(res.data);
        })
        .catch((err) => {
            console.log(err);
        })

    }, tickTime);
}









