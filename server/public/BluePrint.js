


// BUG LIST

// [BUG] Clicking first created room first time moves the room to upper left corner
// [BUG] Dragging room and simultaneously right clicking OR moving mouse away from vieport
// will end to infinite dragging
// [BUG] Room list panel needs to be scrollable 


// MISSING FEATURES

// [FEATURE] Pressing ESC drops current dragged items




// HTML document's body
//var body = $('body');

// Room array
let rooms = [];

// Is user currently dragging element
isDragging = false;

// Is user currently resizing element
isResizing = false;

// Grid in which to snap the room when dragged
dragSpacing = 10;

// Coordinates used to make dragging more fluent
dragX = 0;
dragY = 0;
dragSet = false;

// Room resize button size
resizeSize = 4;

// Dragging room's id
let roomID = 0;

// Width and height text field
let roomWidthtxt = $('#room_width_txt');
let roomHeighttxt = $('#room_height_txt');

// Panel for room to be listed
let roomPanel = $('#roomPanel');

// Mouse location x and y in viewport
let mouse = {
    x: 0,
    y: 0
}

// Room configs
let roomConf = {
    unselectColor: '555',
    selectColor: 'f74e1e'
}

// Dragged object id
let draggedObjID = '';

// Dragging interval
let draggingObjInterval = 0;

// Blueprint element types
const TYPE_ROOM = 1;
const TYPE_SENSOR = 2;

// Sensor blueprint size
sensorSize = 30;

// Whether input fields have been created
inputFieldsCreated = false;

// Wether resize handle has been created
let resizeHandleCreated = false;

// Top left coordinates of the blueprint
let bprintMinX = 420;
let bprintMinY = 280;

// class for Room
class Room {
    constructor(index, type, id, width, height) {
        this._index = index;
        this._X = 600;
        this._Y = 300;
        this._width = width;
        this._height = height;
        this._listItem = 0;
        this._type = type;
        this._ID = id;
        this._isNew = true;
        this._layoutID = null;
    }
}

// Listen "add room" button
// When clicked create new room
$('#add_room_btn').click(function (e) {
    // n for length of rooms array
    let n = rooms.length;

    //If room width is less than 100 then change it to 100
    if (roomWidthtxt.val() < 100) {
        $('#room_width_txt').val("100")
        roomWidthtxt = $('#room_width_txt');
    }
    //If room height is less than 100 then change it to 100
    if (roomHeighttxt.val() < 100) {
        $('#room_height_txt').val("100")
        roomHeighttxt = $('#room_height_txt');
    }
    // get width and height from viewport text fields
    let width = snapToGrid(roomWidthtxt.val(), dragSpacing);
    let height = snapToGrid(roomHeighttxt.val(), dragSpacing);

    // Create new room and pass 
    let room = new Room(n, TYPE_ROOM, null/*"huone#" + n*/, width, height);

    roomID = room;
    
    // Push new room to rooms array
    rooms.push( room );
    
    createRoomElement(n);
    
    if( inputFieldsCreated == false )
    { createElementProperties(); }
});

// Adding a sensor to the blueprint
$('#add_sensor_btn').click(function (e) {
    // n for length of rooms array
    let n = rooms.length;

    // Create new room and pass 
    let room = new Room(n, TYPE_SENSOR, null/*"sensori#" + n*/, 0, 0);

    roomID = room;
    
    // Push new room to rooms array
    rooms.push( room );
    
    createRoomElement(n);

    if( inputFieldsCreated == false )
    { createElementProperties(); }
});


// Listen all clicks made inside viewport
$(body).click(function (e) {

    // Check clicked element class
    if (e.target.className == 'roomBox') {
        // Get index from id
        roomID = e.target.id.split('room')[1];

        let r = rooms[roomID];

        // Get room's width and length
        let width = r._width
        let height = r._height

        let x = r._X;
        let y = r._Y;

        // Update position of room resize button
        if( r._type == TYPE_ROOM )
        {
            $('#RoomResize').css({
                'margin-left': x + parseInt(width) - resizeSize / 2+'px',
                'margin-top': y + parseInt(height) - resizeSize / 2 +'px'
            });
        }

    }

});

// Listen when mouse click is clicked/ mousedown
$(body).mousedown(function (e) { 

    // Check clicked element class
    if (e.target.className == 'roomBox') {
        e.preventDefault();
        $("#RoomResize").css({"display":"block"});
    
        // Get index from id
        roomID = e.target.id.split('room')[1];

        $('#roomName').val(rooms[roomID]._ID);
        $('#roomWidth').val(rooms[roomID]._width);
        $('#roomHeight').val(rooms[roomID]._height);

        if (draggedObjID) {
            $('#'+draggedObjID).css({
                'background': '#'+roomConf.unselectColor
            });
        }


        isDragging = true;
        draggedObjID = e.target.id;


        $('#'+draggedObjID).css({
            'background': '#'+roomConf.selectColor
        });

        // Smooth dragging
        if( dragSet == false )
        {
            dragX = e.clientX - $('#'+draggedObjID).offset().left;
            dragY = e.clientY - $('#'+draggedObjID).offset().top;
            dragSet = true;
        }

        $(body).mousemove((e) => {

            draggingObject(e);
        });
    }

});

// SQL-query generation
$("#gen_sql_btn").click(function(e){

    let r_data = [];
    let s_data = [];
    let r, rx, ry, rw, rh, rid, rjson, lid, rnew;

    // Insert room data to a json
    for( let i = 0; i < rooms.length; i++ )
    {
        r = rooms[i];

        // Element not found, skip
        if( r === undefined )
        { continue; }

        rid = r._ID;
        
        // An element has not ID
        if( rid === null )
        {
            notificationBanner.displayNotification("Varmista, että kaikilla huoneilla/sensoreilla on nimet!", NTFI_COL_FAIL);
            return;
        }

        rx = r._X;
        ry = r._Y;
        rw = r._width;
        rh = r._height;
        rnew = r._isNew;

        if( editingBlueprint == false )
        { lid = i + 1; }
        else
        { lid = r._layoutID; }

        rjson = { rID: rid, rX: rx, rY: ry, rWidth: rw, rHeight: rh, layID: lid, isNew: rnew };

        switch( r._type )
        {
            case TYPE_ROOM:
                r_data.push(rjson);
                break;
            
            case TYPE_SENSOR:
                s_data.push(rjson);
                break;
        }
    }

    let data = { editing: editingBlueprint, roomData: r_data, sensorData: s_data };

    // Send data to server
    let postData = POSTAjaxCall('updateBlueprint', data);
        postData.then((resolve) => {
            console.log(resolve);

            if( resolve != undefined )
            {
                // Query couldn't be executed
                if( resolve.result === "fail" )
                { notificationBanner.displayNotification("Pohjapiirrustuksen tallennus epäonnistui!", NTFI_COL_FAIL); }
                else
                {
                    // Adjust layout IDs of newly added elements
                    if( resolve.result != "success" )
                    {
                        let rset = resolve;
                        for( let i in resolve )
                        {
                            for( let j in rooms )
                            {
                                let rm = rooms[j];

                                if( rm._ID == rset[i].eid )
                                {
                                    rm._layoutID = rset[i].lid;
                                    rm._isNew = false;
                                }
                            }
                        }
                    }

                    // A blueprint was being edited, return to Building surveillance
                    if( editingBlueprint == true )
                    {
                        editingBlueprint = false;

                        // Update blueprint

                        drawBuildingBlueprint(survSelectedBuilding);
                        $("#buildingSurveillance_btn").trigger("click");
                    }

                    notificationBanner.displayNotification("Pohjapiirrustuksen tallennus onnistui!");
                }
            }


        }, (err) => {
            console.log('error!');
            notificationBanner.displayNotification("Pohjapiirrustuksen tallennus epäonnistui!", NTFI_COL_FAIL);
    });
});

// Listen when mouse click is released
$(body).mouseup(function (e) { 

    // Check if dragging was true
    if (isDragging == true) {
        isDragging = false;

        // Clear interval
        //clearInterval(draggingObjInterval);
        draggingObjInterval = 0;
        dragSet = false;

        let room = rooms[roomID];
        room._X = $('#'+draggedObjID).offset().left;
        room._Y = $('#'+draggedObjID).offset().top - dragSpacing - 3;


        console.log('end dragging');
        console.log(rooms[roomID]._Y)

    }

    isResizing = false;

});


// Dragging element
function draggingObject(e) {

    if( isDragging == true )
    {
        mouse.x = e.clientX;
        mouse.y = e.clientY;

        // Get room's width and length
        let width = rooms[roomID]._width
        let height = rooms[roomID]._height
        let x = mouse.x - dragX;
            x = Math.max(bprintMinX, snapToGrid(x, dragSpacing));
        let y = mouse.y - dragY;
            y = Math.max(bprintMinY, snapToGrid(y, dragSpacing) - dragSpacing);

        // Move element where mouse coordinates are
        $('#'+draggedObjID).css({
            'margin-left': x+'px',
            'margin-top': y+'px'
        });

        // Update position of room resize button
        if( rooms[roomID]._type == TYPE_ROOM )
        {
            $('#RoomResize').css({
                'margin-left': x + parseInt(width) - resizeSize / 2+'px',
                'margin-top': y + parseInt(height) - resizeSize / 2 +'px'
            });
        }
    }
}


// Create new element
function createRoomElement(index) {
    let newRoom = document.createElement('div');
    newRoom.id = 'room'+index;
    newRoom.className = 'roomBox'

    room = rooms[index];

    let width = room._width;
    let height = room._height;
    let marginLeft = room._X;
    let marginTop = room._Y;
    let type = room._type;

    // Append new element into body
    //body.append(newRoom);
    let d = body
    d.prepend(newRoom);

    // Create resize handle
    if( resizeHandleCreated == false )
    {
        let rsize = createElement("div");
            rsize.id = "RoomResize";
            rsize.style.display = "none";
        
            rsize.addEventListener("mousedown", function (e) { 

                if( rooms[roomID]._type == TYPE_ROOM )
                {
                    isResizing = true;
                    // If mouse is moved update mouse parameters
                    $(body).mousemove(function (e) { 
            
                        if( isResizing == true )
                        {
                        mouse.x = e.clientX;
                        mouse.y = e.clientY;
                        
            
                        // Get room's width and height
                        let room = rooms[roomID];
                        let rx = room._X;
                        let ry = room._Y;
            
                        // Snap to grid
                        let mx = mouse.x;
                            mx = snapToGrid(mx, dragSpacing);
                        let my = mouse.y;
                            my = snapToGrid(my, dragSpacing);
            
                        let nw = mx - rx;
                        let nh = my - ry;
            
                        // Update room size
                        $('#room'+roomID).css({
                            'width': nw+'px',
                            'height': nh+'px'
                        });
            
                        // Update room object
                        room._width = nw;
                        room._height = nh;
            
                        // Update resize button's position
                        $('#RoomResize').css({
                            'margin-left': mx+'px',
                            'margin-top': my+'px'
                        });
                    }
                    });
                }
            
            
            });
        
        $("body").prepend(rsize);

        resizeHandleCreated = true;
    }

    // Apply style to new element
    switch( type )
    {
        case TYPE_ROOM:
            $('#room'+index).css({
                'width': width+'px',
                'height': height+'px',
                'margin-left': marginLeft+'px',
                'margin-top': marginTop+'px'
            });

            /*let roomLabel = document.createElement('div');

            roomLabel.innerHTML = 'room#'+index;
            roomLabel.className = 'roomNameLabel';



            newRoom.append(roomLabel);*/
            break;
        
        case TYPE_SENSOR:
            $('#room'+index).css({
                'width': sensorSize+'px',
                'height': sensorSize+'px',
                'margin-left': marginLeft+'px',
                'margin-top': marginTop+'px',
                'background':'rgb(255,0,0)',
                'z-index':'1'
            });
            break;
    }
}


// Create input field
function createInputField(elementID, elementClass) {
    let inputField = document.createElement('input');

    inputField.id = elementID;
    inputField.className = elementClass;
    inputField.placeholder = elementID;

    return inputField;
}

// Hides/displays blueprint software elements
function hideBlueprintElements(hide)
{
    if( hide == true )
    {
        $("#RoomResize").css({"display":"none"});
        $(".roomBox").css({"display":"none"});

        if (draggedObjID) {
            $('#'+draggedObjID).css({
                'background': '#'+roomConf.unselectColor
            });
        }
    }
    else
    {
        $(".roomBox").css({"display":"block"});
    }
}

// Create element properties

function createElementProperties()
{
    let newItem = document.createElement('li');

        newItem.id = 'roomItem';
        newItem.className = 'roomItem';

        infoPanel = document.createElement('div');
        infoPanel.className = 'roomInfoPanel'

        newItem.append(infoPanel);

    roomPanel.append(newItem);


        inpName = createElement('div');
        inpName.id = "roomName";
        inpName.innerHTML = "Valitse";

        inpWidth = createInputField('roomWidth');
        inpHeight = createInputField('roomHeight');
        btnRemove = createElement('div');
        btnRemove.id = "roomRemove";
        btnRemove.innerHTML = "Poista";

        infoPanel.append(inpName);

        infoPanel.append(createElement("br"));
        infoPanel.append(inpWidth);
        infoPanel.append(inpHeight);
        infoPanel.append(btnRemove);



    // Add eventlisteners to input fields
        inpWidth.addEventListener('keyup', function() {
            let id = roomID;
            let width = snapToGrid($(this).val(), dragSpacing);

            $('#room'+id).css({
                'width': width,
            });

            rooms[roomID]._width = width;
        });

        inpHeight.addEventListener('keyup', function() {
            let id = roomID;
            let height = snapToGrid($(this).val(), dragSpacing);

            $('#room'+id).css({
                'height': height,
            });

            rooms[roomID]._height = height;
        });

    inpName.addEventListener('click', function() {
        /*let id = this.parentNode.parentNode.id.split('roomItem')[1];
        let name = $(this).val();


        rooms[roomID]._ID = name;*/

        if( rooms[roomID]._type == TYPE_ROOM )
        { selectingFor = 3; }

        if( rooms[roomID]._type == TYPE_SENSOR )
        { selectingFor = 4; }

        clickAuthentic = false;

        $("#cancelSelection").css({"display":"block"});
        $("#dbManagement_btn").trigger("click");
    });

    // 
    btnRemove.addEventListener("click", function() {

        let id = rooms[roomID];

        rooms[roomID] = undefined;
        $("#room" + roomID).remove();

        // Element is not in database -> exit
        if( id._isNew == true )
        { return; }

        // Remove element from database
        let postData = POSTAjaxCall("deleteFromBlueprint", {elmType: id._type, elmID: id._layoutID});
        postData.then((resolve) => {
            console.log(resolve);
        });
    });

    inputFieldsCreated = true;
}

// Returns 'rooms'-array index of an element with given id
// Otherwise returns -1
function findElementByID(id)
{
    for( let i in rooms )
    {
        if( rooms[i]._ID == id )
        { return i; }
    }

    return -1;
}

// Snaps a given value to a grid of given size
function snapToGrid(val, g)
{ return Math.floor(val / g) * g; }