

let body = $('body');
let tableWrapper = $('#tableWrapper');


// ID of the tab that is currently active
let activeTab = "";

// Name for current database table
let currentTable = 'property_group';

// Chart on sensor comparison- tab
let sensorComparisonChart;

// A database entry is being modified
let beingModified = false;

// Selected items' IDs
let selectedItem = [];

// Whether a sensor is being selected for custom alert creation
let selectingFor = 0;

// Column being dragged
let draggingColumn = -1;

// Tset ticker for tracking
let dragTick = 0;
let dragTickFreq = 40;

// Column that could be dragged if mouse was clicked
let canDragColumn = -1;

// Holds the building that has been selected for surveillance page
let survSelectedBuilding;

// ID of the user in the databse
let staffID = 1;

// Resultset received when 'My buildings' is clicked on My profile
let myBuildings;

// Whether tab was clicked via code or by user
clickAuthentic = true;

// First time accessing sensor comparison page
let initialDateRangeSet = false;

// Keys of tables
let tableKeys = new Map();
tableKeys.set("property_group", ["Property_group_ID", "Property_group_name"]);
tableKeys.set("building", ["Building_ID", "Building_name", "Building_address", "Building_description", "Property_group_ID"]);
tableKeys.set("room", ["Room_ID", "Room_name", "Building_ID"]);
tableKeys.set("sensor", ["Sensor_ID", "Sensor_description", "Building_ID", "Room_ID", "Sensor_type_ID"]);

// Whether a blueprint is being edited
let editingBlueprint = false;

// Notification banner colors
const NTFI_COL_FAIL = "tomato";

// Sort table stuff
let sortTableID = 0;
let sortACS = true;

const lang =
{
	"Building_ID": "Rakennus (ID)",
	"Building_name": "Nimi",
	"Building_address": "Osoite",
	"Building_description": "Kuvaus",
	"Property_group_ID": "Kiinteistöryhmä (ID)",
	"Property_group_name": "Kiinteistöryhmä",
	"Room_ID": "Huone (ID)",
	"Room_name": "Nimi",
	"Sensor_ID": "Sensori (ID)",
	"Sensor_description": "Kuvaus",
	"Sensor_type_ID": "Sensorityyppi (ID)"
}

// Notification banner class
class NotificationBanner
{
    constructor()
    {
        this._text = "";            // text to display
        this._displayTime = 3000;   // time to display for (milliseconds)
        this._fadeOutTime = 1000;   // time it takes to fade out (milliseconds)
        this._colorBack = "green";  // default background color of the banner
        this._bannerID = $("#notificationBanner");  // banner element
    }

    // Display notification
    displayNotification(text, color)
    {
        let bcol = this._colorBack;

        if( color != undefined )
        { bcol = color; }

        let nb = this._bannerID;
        nb.css({"display":"block", "background":bcol});
        nb.html(text);

        let inter = setInterval(() => {

            nb.fadeOut(this._fadeOutTime);
            clearInterval(inter);
        }, this._displayTime);
    }
}

notificationBanner = new NotificationBanner();

class DataTable {
    constructor() {
        this._menuItems = [
            'Kiinteistöryhmät',
            'Kiinteistöt',
            'Huoneet',
            'Sensorit'
        ]
        this._menuIDs = [
            'property_group',
            'building',
            'room',
            'sensor'
        ]
        this._data = {
            headers: [],
            keys: [],
            visible: [],
            resultSet: []
        };

        this._tableSunHeader;

        this._menu = this.buildMenu();
        this._filter = this.buildFilter();
    };

    // Sort table ala MAGGGYVER
    sortTable(id) {
        sortTableID = id;

        this._data.resultSet.sort((a, b) => {
            let n1 = a[this._data.keys[id]];
            let n2 = b[this._data.keys[id]];

            if (sortACS == false) {
                if (n1 < n2) { return -1 }
                if (n1 > n2) { return 1 }
            }
            if (sortACS == true) {
                if (n1 < n2) { return 1 }
                if (n1 > n2) { return -1 }
            }
            return 0;
        });

        if (sortACS == true) {
            sortACS = false
        } else {
            sortACS = true;
        }
        this.buildFilter();
        this.buildTableContent(this._data);

    }

    // Render table content
    showRecords(record) {

        beingModified = false;
        let getData = GetAjaxCall('records', { type: record });
        getData.then((result) => {

            this._data = result;

            if ($('#tableContent')) {
                $('#tableContent').remove();
            }

            this.tableNewRecord();

            this.buildFilter();
            this.buildTableContent(result);

        }, (err) => {
            console.log('error!');
        });
    }

    tableNewRecord() {
        if ($('#newRecordPanel')) {
            $('#newRecordPanel').remove();
        }

        let newRecord = createElement('div', 'newRecordPanel');

        for (let i in this._data.keys) {
            let input = createElement('input', 'nrec' + i, "nrecInput");
            input.placeholder = lang[this._data.keys[i]];

            appendTo(newRecord, input);
        }

        let submit = createElement('input', 'addNewRecord_btn');
        submit.type = 'submit';
        submit.value = 'Lisää'

        let disc = createElement('input', 'discardChanges_btn');
        disc.type = 'submit';
        disc.value = 'Hylkää muutokset'


        // Send data to server
        submit.addEventListener('click', () => {
            // An entry is not being modified -> add new a new entry
            if( beingModified == false )
            {
                let fieldCount = 0;
                let fieldData = [];

                while (fieldCount != newRecord.children.length - 1) {
                    let item = newRecord.children[fieldCount].value;

                    fieldData.push(item);
                    fieldCount++;
                }

                fieldData.pop();

                let data = {
                    table: currentTable,
                    fields: fieldData,
                }

                let postData = POSTAjaxCall('addRecord', data);
                postData.then((resolve) => {
                    // DEEEBUGGG!!!!!
                    console.log(resolve);
                    if( resolve.result === "fail" )
                    { notificationBanner.displayNotification("Tiedon lisäys epäonnistui!", NTFI_COL_FAIL); }
                    else
                    { notificationBanner.displayNotification("Tiedon lisäys onnistui!"); }

                }, (err) => {
                    console.log('error!');
                    notificationBanner.displayNotification("Tiedon lisäys epäonnistui!", NTFI_COL_FAIL);
                });
            }
            else
            {
                // An entry is being modified

                let data = {
                    table: currentTable
                };

                // Get input
                /*$(".nrecInput").each(function(){

                    data[this.placeholder] = this.value;
                });*/

                let k = tableKeys.get(currentTable);
                for( let i in k )
                {
                    data[k[i]] = $("#nrec"+i).val();
                }

                // Send changes to server
                let postData = POSTAjaxCall('modRecord', data);
                postData.then((resolve) => {

                    console.log(resolve);
                    if( resolve.result === "fail" || resolve.affectedRows == 0 )
                    { notificationBanner.displayNotification("Tiedon muokkaus epäonnistui! Yritettiin muokata tietoa, jota ei ole olemassa!", NTFI_COL_FAIL); }
                    else
                    { notificationBanner.displayNotification("Tiedon muokkaus onnistui!"); }

                }, (err) => {
                    console.log('error!');
                    notificationBanner.displayNotification("Tiedon muokkaus epäonnistui!", NTFI_COL_FAIL);
                });
            }
        });

        // Discard changes
        disc.addEventListener('click', () => {
            beingModified = false;

            $(".nrecInput").val("");
            $("#" + disc.id).hide();
            $("#addNewRecord_btn").val("Lisää");
        });

        appendTo(newRecord, submit);
        appendTo(newRecord, disc);
        appendTo(tableWrapper, newRecord);

        // Hide 'discard changes'- button for now
        $("#" + disc.id).hide();
    }

    // Render table content
    showRecordsNext(record) {

        beingModified = false;
        let table;
        switch (currentTable) {
            case 'property_group':
                table = 'building';
            break;
            case 'building':
                table = 'room'
            break;
            case 'room':
                table = 'sensor'
            break;
            case 'sensor':
                table = 'sensor'
            break;
            default:
            break;
        }

        selectedItem = [];

        $('.tableLi_CheckBox').each((index, element) => {
            let id = null;

            if (index == 0) {
                return;
            }

            if (element.checked) {
                id = index - 1;
            }

            if (id || id == 0) { // Interesting...
                let item = this._data.resultSet[id][this._data.keys[0]];
                selectedItem.push(item);
            }

        });

        if (currentTable == 'sensor') {

            switch( selectingFor )
            {
                // Selecting for sensor comparison
                case 0:

                    // Goto Sensor comparison
                    $('#comparison_btn').trigger('click');
                    $('#compGetSensors').trigger('click');
                    return;
                    break;
                
                // Selecting for custom alert
                case 1:

                    // Select only one sensor
                    if( selectedItem.length > 1 )
                    { notificationBanner.displayNotification("Valitse vain yksi sensori!", NTFI_COL_FAIL); }
                    else
                    {
                        // Successful -> return to custom alert creation screen
                        $('#allAlerts_btn').trigger('click');
                        selectingFor = 0;
                        $('#allAlertssensorName').html(selectedItem[0]);
                    }

                    return;
                    break;
                
                // Selecting for sensor blueprint
                case 4:

                    // Select only one sensor
                    if( selectedItem.length > 1 )
                    { notificationBanner.displayNotification("Valitse vain yksi sensori!", NTFI_COL_FAIL); }
                    else
                    {
                        // Check if the room has already been added
                        if( findElementByID(selectedItem[0]) < 0 )
                        {
                            $('#2derSoftware_btn').trigger('click');
                            selectingFor = 0;
                            rooms[roomID]._ID = selectedItem[0];

                            $("#RoomResize").css({"display":"block"});

                            if (draggedObjID) {
                                $('#'+draggedObjID).css({
                                    'background': '#'+roomConf.selectColor
                                });
                            }
                        }
                        else
                        { notificationBanner.displayNotification("Valitsemasi sensori löytyy jo pohjapiirrustuksesta!", NTFI_COL_FAIL); }
                    }

                    return;
                    break;
            }
        }

        if( currentTable == "building" )
        {
            switch( selectingFor )
            {
                // Selecting for Building surveillance- page
                case 2:

                    // Select only one building
                    if( selectedItem.length == 1 )
                    {
                        survSelectedBuilding = selectedItem[0];
    
                        $('#buildingSurveillance_btn').trigger('click');
                        //$(".roomBox").remove();
                        drawBuildingData(survSelectedBuilding);
                        selectingFor = 0;
                    }
                    else
                    { notificationBanner.displayNotification("Valitse vain yksi kiinteistö!", NTFI_COL_FAIL); }

                    return;
                    break;
            }
        }

        if( currentTable == "room" )
        {
            switch( selectingFor )
            {
            // Selecting for blueprint
            case 3:
                    
                // Select only one room
                if( selectedItem.length == 1 )
                {
                    // Check if the room has already been added
                    if( findElementByID(selectedItem[0]) < 0 )
                    {
                        $('#2derSoftware_btn').trigger('click');
                        selectingFor = 0;
                        rooms[roomID]._ID = selectedItem[0];

                        $("#RoomResize").css({"display":"block"});

                        if (draggedObjID) {
                            $('#'+draggedObjID).css({
                                'background': '#'+roomConf.selectColor
                            });
                        }
                    }
                    else
                    { notificationBanner.displayNotification("Valitsemasi huone löytyy jo pohjapiirrustuksesta!", NTFI_COL_FAIL); }
                }
                else
                { notificationBanner.displayNotification("Valitse vain yksi huone!", NTFI_COL_FAIL); }

                return;
                break;
            }
        }


        currentTable = table;


        let getData = GetAjaxCall('records', { type: table, selectedItem });
        getData.then((result) => {

            this._data = result;

            if ($('#tableContent')) {
                $('#tableContent').remove();
            }

            this.tableNewRecord();
            this.buildFilter();
            this.buildTableContent(result);

        }, (err) => {
            console.log('error!');
        });
    }

    // Building headerbat for table
    buildTableContent(body) {


        if ($('#tableContent')) {
            $('#tableContent').remove();
        }

        let content = createElement('div', 'tableContent');
        let ul = createElement('ul');

        appendTo(content, ul);

        // header and item 'builders' should be merged to one function, no more repeating code!

        // Build header for table
        let li = createElement('li', '', 'tableLiHeader');

        let selectBox = createElement('input', '', 'tableLi_CheckBox');
        selectBox.type = 'checkbox';

        // Handle selecting/ deselecting all
        selectBox.addEventListener('click', () => {
            let selecting = true;
            $('.tableLi_CheckBox').each((index, element) => {
                if (index == 0) {
                    if (element.checked == true) {
                        selecting = true
                    } else {
                        selecting = false;
                    }
                }

                element.checked = selecting;
            });
        });

        appendTo(li, selectBox);


        let sortID = 0;
        for (let i in body.headers) {    
            // MacGyver filtering

            if (body.visible[i] == true) {
                let span = createElement('span', 'sort'+sortID);
                let text = lang[body.keys[i]];

                span.addEventListener('click', () => {
                    let id = span.id.split('sort')[1];
                    this.sortTable(id);
                })

                appendTo(span, text);
                appendTo(li, span);
            }

            sortID++;
        }

        appendTo(ul, li);


        // Build each item
        for (let i in body.resultSet) {
            let li = createElement('li', '', 'tableLiItem');

            let selectBox = createElement('input', 'item'+body.resultSet[i][body.keys[0]], 'tableLi_CheckBox');
            selectBox.type = 'checkbox';

            selectBox.addEventListener('click', () => {

                // Move the entry to the input fields
                for( let j in body.keys )
                { $('#nrec' + j).val(body.resultSet[i][body.keys[j]]); }

                beingModified = true;
                $("#discardChanges_btn").show();
                $("#addNewRecord_btn").val("Tallenna muutokset");
            })

            appendTo(li, selectBox);

            for (let j in body.keys) {

                // Server response modding needed!
                // Add what kind of field it is => example parse dats
                // Add field size option on each cell => example ID field should be smaller than email
                
                if (body.visible[j] == true) {
                    let span = createElement('span');
                    let text = body.resultSet[i][body.keys[j]];

                    span.addEventListener('click', () => {

                        // SELECT EACH ROW WITH SAME VALUE?
                        // Might be difficult to add
                        // Since whole system might crumble (needs fixing/ changes)

                    })

                    appendTo(span, text);
                    appendTo(li, span);
                }
            }
            appendTo(ul, li);
        }
        appendTo(tableWrapper, content);




        switch (currentTable) {
            case this._menuIDs[0]:
                $('#itemsCount_'+this._menuIDs[0]).html(this._data.resultSet.length);
            break;
            case this._menuIDs[1]:
                $('#itemsCount_'+this._menuIDs[1]).html(this._data.resultSet.length);
            break;
            case this._menuIDs[2]:
                $('#itemsCount_'+this._menuIDs[2]).html(this._data.resultSet.length);
            break;
            case this._menuIDs[3]:
                $('#itemsCount_'+this._menuIDs[3]).html(this._data.resultSet.length);
            break;
            default:
            break;

        }


    }

    // Build header
    buildMenu() {
        let header = createElement('div', 'tableHeader');
        //header.id = 'tableHeader';

        let ul = createElement('ul');

        appendTo(header, ul);

        for (let i in this._menuItems) {
            let li = createElement('li', this._menuIDs[i]);

            // REQUIRES event click for adding new record
            // Doesn't have functionality!
            // Requires work!
            let span = createElement('span', '', 'tableNewRecord_btn');
            appendTo(span, '+');
            appendTo(li, span);

            // Add event click for each header => get records from database
            // Requires work! Look "showRecords"
            span = createElement('span', '', 'tableRecordTitle');
            span.addEventListener('click', () => {
                this.showRecords(this._menuIDs[i]);
                currentTable = this._menuIDs[i];
            });

            let text = this._menuItems[i];
            appendTo(span, text);
            appendTo(li, span);

            // Number of records selected
            // Requires work!
            span = createElement('span', 'itemsCount_'+this._menuIDs[i], 'tableRecordTitle2');
            text = this._data.resultSet.length;
            appendTo(span, text);
            appendTo(li, span);

            // Dropdown button for record headers
            // Requires work!
            // Doesn't have functionality!
            span = createElement('span', '', 'tableNewRecord_btn');

            span.addEventListener('click', () => {
                if ($('#filterPanel').css('visibility') == 'hidden') {
                    $('#filterPanel').css({ 'visibility': 'visible' })
                } else {
                    $('#filterPanel').css({ 'visibility': 'hidden' })
                }
            });

            appendTo(span, '>');
            appendTo(li, span);

            appendTo(ul, li);
        }

        appendTo(tableWrapper, header);


        let nextBtn = createElement('div', "tableNext_btn");


        nextBtn.addEventListener('click', () => {
            this.showRecordsNext();
        });

        appendTo(nextBtn, 'Siirry');
        appendTo(tableWrapper, nextBtn);

    }

    // Build filter panel
    buildFilter() {
        // Check if filter panel exist and remove it
        if ($('#filterPanel')) {
            $('#filterPanel').remove();
        }

        // Panel for seacrhing single row
        let searchPanel = createElement('div', 'searchPanel');

        let filter = createElement('div', 'filterPanel');
    
        let div = createElement('div', 'filterOptionsLine');

        // Index used for toggling filtered options
        let filterCounter = 0;

        // Build every filtered option
        for (let i in this._data.keys) {

            // Create input fields for each column
            let searchField = createElement('input', 'search' + i, 'columnSearchField');
            searchField.placeholder = lang[this._data.keys[i]];

            appendTo(searchPanel, searchField);

            let btn = createElement('div', 'filter'+filterCounter, 'filter_btn');

            // Change button color
            if (this._data.visible[i] == true) {
                btn.style.background = 'steelblue';
            } else {
                btn.style.background = 'tomato';
            }
        
            // Add eventlistener for button
            btn.addEventListener('click', () => {
                // Get index from button element's id
                let index = btn.id.split('filter')[1];

                // Change visibility to opposite
                if (this._data.visible[index] == false) {
                    this._data.visible[index] = true
                } else {
                    this._data.visible[index] = false
                }

                // Build filter
                this.buildFilter();

                // Build table content
                this.buildTableContent(this._data);
            });

            // Add text to button
            let txt = lang[this._data.keys[i]];
            appendTo(btn, txt);
            appendTo(div, btn);

            filterCounter++;
        }
        // Add filter panel to table
        appendTo(filter, div);

        // Add search button
        let searchbtn = createElement('button', "searchColumns");
        searchbtn.innerHTML = 'Etsi';

        // Add cancel button to cancel selections
        let cancelbtn = createElement("button", "cancelSelection");
        cancelbtn.innerHTML = "PERUUTA";
        if( selectingFor <= 0 )
        { cancelbtn.style.display = "none"; }

        // Search button click
        searchbtn.addEventListener('click', () => {
                
            // Iterate through rows
            $(".tableLiItem").each(function(index, element){

                let ch = $(this).find('span');
                let cols = ch.length;
                let srch;

                element.style.display = "block";

                // Iterate through columns of the row
                for( let i = 0; i < cols; i++ )
                {
                    srch = $("#search" + i).val();

                    // Hide rows that do NOT contain the string
                    if( srch != '' && !ch[i].innerHTML.includes(srch) )
                    { element.style.display = "none"; }
                }
            });
        });

        appendTo(searchPanel, searchbtn);

        // Cancel button click
        cancelbtn.addEventListener('click', () => {
                
            switch( selectingFor )
            {
                // Return to custom alert creation
                case 1:
                    $('#allAlerts_btn').trigger('click');
                    $('#createCustomAlert').trigger('click');
                    cancelbtn.style.display = "none";
                    break;
                
                // Return to building surveillance
                case 2:
                    $('#buildingSurveillance_btn').trigger('click');
                    cancelbtn.style.display = "none";
                    break;
                
                // Return to blueprint software
                case 3:
                case 4:
                    $("#2derSoftware_btn").trigger("click");
                    cancelbtn.style.display = "none";
                    break;
            }
        });

        appendTo(searchPanel, createElement("br"));
        appendTo(searchPanel, cancelbtn);

        appendTo(filter, searchPanel);
        appendTo(tableWrapper, filter);
    }


}

// Class for rooms in the blue print
class BprintRoom
{
    constructor(el, x, y, w, h)
    {
        this.bpX = x;
        this.bpY = y;
        this.bpWidth = w;
        this.bpHeight = h;
        this.bpElement = el;
    }

    // Update position of the representing element
    updatePosition()
    {
        $("#" + el).css({
            "margin-left": this.bpX,
            "margin-top": this.bpY,
            "width": this.bpWidth,
            "height": this.bpHeight
        });
    }
}


// Append child to parent
function appendTo(parent, child) {
    parent.append(child);
}

// Create element
function createElement(type, id, c) {
    let element = document.createElement(type);

    if( typeof id != 'undefined')
    { element.id = id; }

    if( typeof c != 'undefined')
    { element.className = c; }

    return element;
}


// Render content list
function renderList(data) {
    // Empty content
    $('#contentItems').html('');

    // Headers for data
    let params = [
        'sensorID',
        'sensorType',
        'sensorAddDate'
    ]

    // Create header for contentlist
    let listItem = createElement('li');
    for (j in params) {
        let span = createElement('span');
        let text = params[j];
        span.append(text);
        listItem.append(span);
    }

    $('#contentItems').append(listItem);

    // Loop thru list items
    for (i in data) {
        let listItem = createElement('li');

        for (j in params) {
            let span = createElement('span');
            let text = data[i][params[j]];
            span.append(text);
            listItem.append(span);
        }

        $('#contentItems').append(listItem);

    }

}


// Return to table
$('#compReturnToTable').click(() => {
    $('#dbManagement_btn').trigger('click');
})

// Send changes to server
$('#allAlertsCreate').click(() => {

    let sid = $("#allAlertssensorName").html();
    let vmin = $("#allAlertsSensorMin").val();
    let vmax = $("#allAlertsSensorMax").val();

    // Make sure that min < max
    if( vmin > vmax )
    {
        notificationBanner.displayNotification("Minimi arvo on suurempi kuin maksimi!", NTFI_COL_FAIL);
        return;
    }

    // Check if required inputs have been given
    if ( vmin != "" && vmax != "" && sid != "" )
    {

        // Send new alert data to server
        let postData = POSTAjaxCall('addCustomAlert', {sensID: sid, valueMin: vmin, valueMax: vmax});
        postData.then((resolve) => {

            console.log(resolve);
            
            if( resolve.result === "fail" )
            { notificationBanner.displayNotification("Hälytyksen lisäys epäonnistui!", NTFI_COL_FAIL); }
            else
            { notificationBanner.displayNotification("Hälytyksen lisäys onnistui!"); }

        }, (err) => {
            console.log('error!');

            notificationBanner.displayNotification("Hälytyksen lisäys epäonnistui!", NTFI_COL_FAIL);
        });
    }
    else
    { notificationBanner.displayNotification("Joitain arvoista ei ole annettu!", NTFI_COL_FAIL); }
})

// Get all sensors
$("#compGetSensors").click(function (e) {
    let getData = GetAjaxCall('records', { type: 'sensor_value' });
    getData.then((result) => {

        // Destroy the chart if it has already been created in order to draw a new one
        if( typeof sensorComparisonChart != "undefined" )
        { sensorComparisonChart.destroy(); }
        sensorComparisonChart = createChart();

        let rset = result.resultSet;

        let sensors = [];
        let dset;
        let sid;

        // Color blinds and other stuff...
        let colors =    ['firebrick', 'dodgerblue',  'yellow',  'chocolate', 'palegreen', 'cyan', 'lightcoral', 'lawngreen', 'pink', 'darkgreen',
                        'paleturquoise',  'steelblue', 'olive', 'deeppink', 'orange', 'honeydew', 'khaki', 'darkseagreen', 'gold', 'mistyrose', 'limegreen',
                        'slategray', 'blueviolet', 'midnightblue', 'darkslategray', 'cornflowerblue', 'orchid', 'turquoise', 'tomato', 'teal']
        let colorIndex = 0;

        // Cycle through the result set
        for( i in rset )
        {
            sid = rset[i].Sensor_ID;

            if( !sensors.includes(sid) )
            {

                if (selectedItem.length != 0) {
                    // Do nothing, if selected sensor ID doesn't exist
                    if (!selectedItem.includes(sid)) {
                        continue;
                    }
                }

                // Create a new dataset for each sensor

                dset = {
                    label: 'Sensori #' + sid,
                    data: [{x: rset[i].Sensor_value_date, y: rset[i].Sensor_value_atr}],
                    borderWidth: 4,
                    fill: false,
                    backgroundColor: colors[colorIndex],
                    borderColor: colors[colorIndex],
                }

                colorIndex++;

                // Push all values of that sensor to the dataset
                for( j = 1; j < rset.length; j ++ )
                {
                    if( rset[j].Sensor_ID == sid )
                    {
                        dset.data.push({x: rset[j].Sensor_value_date, y: rset[j].Sensor_value_atr});
                    }
                }

                // Push dataset to chart
                sensorComparisonChart.data.datasets.push(dset);
                sensors.push(sid);
            }
        }

        // Update the chart
        sensorComparisonChart.update();

        //if( initialDateRangeSet == false )
        //{
            $("#applyDateRange").trigger("click");
        //    initialDateRangeSet = true;
        //}

    }, (err) => {
        console.log('error!');
    });
    
});

// Clicking on main menu elements
$(".mmItem").click(function (e) {

    $(".mmItem").css({
        'font-weight': 'normal',
        'color': '#434343'
    });

    $("#" + e.target.id).css({
        'font-weight': 'bold',
        'color': '#000000'
    });

    activeTab = "#" + e.target.id;
});

// Apply range to chart
$("#applyDateRange").click(function (e) {
    
    sensorComparisonChart.options.scales.xAxes[0].time.min = $("#dateRangeStart").val();
    sensorComparisonChart.options.scales.xAxes[0].time.max = $("#dateRangeEnd").val();
    
    sensorComparisonChart.update();
});

// Apply quick date ranges

// last week
$("#drangLastWeek").click(function (e) {
    
    setDateRangeSince(7);
    $("#applyDateRange").trigger("click");
});

// last month
$("#drangLastMonth").click(function (e) {
    
    setDateRangeSince(30);
    $("#applyDateRange").trigger("click");
});

// last week
$("#drangLastYear").click(function (e) {
    
    setDateRangeSince(365);
    $("#applyDateRange").trigger("click");
});


// Open custom alert creation
$("#createCustomAlert").click(function (e) {
    
    // Display custom alert creation div
    $("#customAlert").css({
        'display': 'block'
    });

    // Hide all alerts div
    $("#allAlerts").css({
        'display': 'none'
    });
});

// Back to all alerts
$("#backToAllAlerts").click(function (e) {
    
    // Display custom alert creation div
    $("#customAlert").css({
        'display': 'none'
    });

    // Hide all alerts div
    $("#allAlerts").css({
        'display': 'block'
    });
});

// Select sensor for custom alert creation
$("#allAlertsSensorSel").click(function (e) {
    
    clickAuthentic = false;
    $("#cancelSelection").css({"display":"display"});

    selectingFor = 1;
    $('#dbManagement_btn').trigger('click');
});

// Select building for surveillance page
$("#survBuildingSel").click(function (e) {
    
    clickAuthentic = false;
    $("#cancelSelection").css({"display":"display"});

    selectingFor = 2;
    $('#dbManagement_btn').trigger('click');
    let btn = $('#building').children()[1];
    $(btn).trigger('click');
});

// My profile: My property groups
$("#piPropGroups").click(function (e) {
    
    let ddownid = "#ddownPropGroups";

    if( $(ddownid).css('display') == 'none' )
    {
        $(ddownid).css({'display':'block'});

        // Get property group data first
        getUserData(staffID, function(){

            let result = myBuildings;
            let liname = "ddownlipropgroups";
            let listed = [];

            $("." + liname).remove();

            // Insert property groups into drop down menu
            for( let i in result )
            {
                let pgid = result[i].Property_group_ID;
                let pgname = result[i].Property_group_name;

                // Only list unlisted property groups to avoid duplicates
                if( !listed.includes(pgid) )
                {
                    let li = createElement("li", "", liname);
                    li.innerHTML = pgname;
                    appendTo($(ddownid), li);
                    listed.push(pgid);
                }
            }
        });
    }
    else
    { $(ddownid).css({'display':'none'}); }
});

// My profile: My buildings
$("#piProps").click(function (e) {
    
    let ddownid = "#ddownProps";

    if( $(ddownid).css('display') == 'none' )
    {
        $(ddownid).css({'display':'block'});

        // Get building IDs first
        getUserData(staffID, function(){

            let result = myBuildings;
            let liname = "ddownliprops";

            $("." + liname).remove();

            // Insert buildings into drop down menu
            for( let i in result )
            {
                let li = createElement("li", "", liname);
                li.innerHTML = result[i].Building_name;
                appendTo($(ddownid), li);
            }
        });
    }
    else
    { $(ddownid).css({'display':'none'}); }
});

// My profile: My alerts
$("#piAlerts").click(function (e) {
    
    let ddownid = "#ddownAlerts";
    if( $(ddownid).css('display') == 'none' )
    {
        $(ddownid).css({'display':'block'});

        // Building IDs are needed for alert data so get them first
        getUserData(staffID, function(){
            let getData = GetAjaxCall('records', { type: 'sensor_alarm' });
            getData.then((result) => {

                let liname = "ddownlialerts";

                $("." + liname).remove();

                let rset_alerts = result.resultSet;
                let rset_builds = myBuildings;

                // Cycle through alerts to find ones that relevant to the user
                for( let i in rset_alerts )
                {
                    for( let j in rset_builds )
                    {
                        if( rset_alerts[i].Building_ID == rset_builds[j].Building_ID )
                        {
                            let li = createElement("li", "", liname);
                            li.innerHTML = rset_alerts[i].Sensor_alarm_description;
                            appendTo($(ddownid), li);
                        }
                    }
                }
            });
        });
    }
    else
    { $(ddownid).css({'display':'none'}); }
});

// My profile: Saved analyses
$("#piAnalyses").click(function (e) {
    
    let ddownid = "#ddownAnalyses";

    if( $(ddownid).css('display') == 'none' )
    {
        $(ddownid).css({'display':'block'});

        // Get building IDs first
        getUserData(staffID, function(){

                let getData = GetAjaxCall('buildingAnalyses', { propertyGroups: myBuildings });
                getData.then((result) => {

                let liname = "ddownlianalyses";

                $("." + liname).remove();

                // Insert buildings into drop down menu
                for( let i in result )
                {
                    let li = createElement("li", "", liname);
                    li.innerHTML = result[i].Sensor_analysis_name;
                    appendTo($(ddownid), li);
                }
            });
        });
    }
    else
    { $(ddownid).css({'display':'none'}); }
});

// Building surveillance: Edit blueprint
$("#survEditBlueprint").click(function (e) {
    
    $("#2derSoftware_btn").trigger("click");
    $("#RoomResize").css({"display":"none"});
    editingBlueprint = true;

    if( rooms.length > 0 )
    {
        for( let i in rooms )
        {
            if( rooms[i] != undefined )
            { roomID = rooms[i]; }
        }

        if( inputFieldsCreated == false )
        { createElementProperties(); }
    }
});

// Activate the appropriate tab
function activateTab(tabid) {

    // Hide all tabs
    $(".contentClass").css({
        'display': 'none'
    });

    // Only show the correct tab
    $("#" + tabid).css({
        'display': 'block'
    });

    // Hide all blueprint software elements
    hideBlueprintElements(true);

    // Switching tabs through user input will cancel selection
    if( clickAuthentic == true )
    {
        selectingFor = 0;
        $("#cancelSelection").css({"display":"none"});
    }

    clickAuthentic = true;

    switch( tabid )
    {
        // All alerts- tab is active => fetch all alerts and list them
        case "contAllAlerts":
            let getData = GetAjaxCall('records', { type: 'sensor_alarm' });
            getData.then((result) => {

                // Clear div
                $(".alertButton").remove();

                let rset = result.resultSet;

                // List all alerts
                for(i in rset)
                {
                    let a = createElement("div", "alert" + i, "alertButton");
                    let d = new Date(rset[i].Sensor_alarm_date);
                    let dd = d.getDate();
                    let mm = d.getMonth() + 1;
                    let yy = d.getFullYear();
                    let hh = d.getHours();
                    let mn = d.getMinutes();
                    let ss = d.getSeconds();
                    let d_str = dd + "." + mm + "." + yy + "  " + hh + ":" + mn + ":" + ss;

                    a.innerHTML = "<span class='alertButtonNameSp'>" + rset[i].Sensor_alarm_description + "</span><br>" +
                    "<span>" + rset[i].Property_group_name + ", " + rset[i].Building_address + "</span><br>" +
                    "<span class='alertButtonDateSp'>" + d_str + "</span>";

                    $('#allAlerts').append(a);
                }

            }, (err) => {
                console.log('error!');
            });
        break;

        // Sensor data comparison tab is active => create chart
        case "contComparison":
            if( initialDateRangeSet == false )
            {
                // Set default date range
                setDateRangeSince(30);

                initialDateRangeSet = true;
            }
            
        break;

         case "contReports":
            let getReports = GetAjaxCall('records', { type: 'sensor_report' });
            getReports.then((result) => {

                // Clear div
                $(".reportButton").remove();

                let rset = result.resultSet;

                // List all alerts
                for(i in rset)
                {
                    let a = createElement("div", "report" + i, "reportButton");
                    let d = new Date(rset[i].Sensor_report_date);
                    let dd = d.getDate();
                    let mm = d.getMonth() + 1;
                    let yy = d.getFullYear();
                    let hh = d.getHours();
                    let mn = d.getMinutes();
                    let ss = d.getSeconds();
                    let d_str = dd + "." + mm + "." + yy;

                    a.innerHTML = "<br><br><span class='reportButtonNameSp'>" + rset[i].Sensor_report_name + "</span><br><br>" +
                    "<span class='reportButtonDescSp'>" + rset[i].Sensor_report_description + "</span><br><br>" +
                    "<span class='reportButtonDateSp'>" + d_str + "</span>";

                    $('#Reports').append(a);
                }

            }, (err) => {
                console.log('error!');
            });
        break;

        // Load in blueprint software
        case "cont2derSoftware":
            appendTo($("#cont2derSoftware"), $("#BlueprintSoftware"));
            body.prepend($("#RoomResize"));
            hideBlueprintElements(false);
            break;
    }
}

// Adds a zero in front of a date if required
function dateAddZero(d)
{
    if( d < 10 )
    { return d = "0" + d;}
    else
    { return d; }
}


// Build table
new DataTable();

//site opens with profile page selected
document.addEventListener("DOMContentLoaded", function(event) { 
    document.getElementById("myProfile_btn").click();
 });

//help overlay functions to make them go on and off
function helpOverlayOn() {
    document.getElementById("helpoverlay").style.display = "block";
} 
function helpOverlayOff() {
    document.getElementById("helpoverlay").style.display = "none";
}
function helpSensorOverlayOn() {
    document.getElementById("sensorhelpoverlay").style.display = "block";
} 
function helpSensorOverlayOff() {
    document.getElementById("sensorhelpoverlay").style.display = "none";
}
function helpBsOverlayOn() {
    document.getElementById("buildsurvhelpoverlay").style.display = "block";
} 
function helpBsOverlayOff() {
    document.getElementById("buildsurvhelpoverlay").style.display = "none";
}
function helpDDOverlayOn() {
    document.getElementById("roomhelpoverlay").style.display = "block";
} 
function helpDDOverlayOff() {
    document.getElementById("roomhelpoverlay").style.display = "none";
}
function helpDbOverlayOn() {
    document.getElementById("databasehelpoverlay").style.display = "block";
} 
function helpDbOverlayOff() {
    document.getElementById("databasehelpoverlay").style.display = "none";
}


// Create a chart
function createChart()
{
    let ctx = document.getElementById('compSensorChart').getContext('2d');
    let chrt = new Chart(ctx, {
        type: 'line',
        data: { },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    gridLines: {
                        color: '#333' //give the needful color
                    },
                    type: 'time',
                    display: true,
                    distribution: 'linear',
                    time: {
                        parser: 'YYYY/MM/DD',
                        unit: 'day',
                        unitStepSize: 1,
                        min: '',
                        max: '',
                        displayFormats: {
                          'day': 'DD/MM/YYYY'
                        }
                      }
                    }],
                yAxes: [{
                    gridLines: {
                        color: '#333' //give the needful color
                    },
                    display: true,
                    ticks: {
                        beginAtZero: false
                    }
                }]
            }
        }
    });

    return chrt;
}

// Handle dragging a column
$(body).on("mousemove", function(e) {

    let mx = e.clientX;
    let my = e.clientY;
    //let pad = 

    // No column is being dragged -> handle dragging availability
    if( draggingColumn < 0 )
    {
        $(".tableLiHeader").each(function(index, element){

            let sp = $(this).find('span');
            let spans = sp.length;
            let sort;
            let nohover = true;
            let x, ow;
            let y1 = $("#tableContent").offset().top;
            let y2 = $("#tableContent").offset().top;
            let pad = 5;

            // Iterate through columns
            for( let i = 0; i < spans; i++ )
            {
                sort = $("#sort" + i);

                if( sort.length == 0 )
                { continue; }

                x = sort.offset().left
                ow = sort.outerWidth();

                // Mouse hovering near column edges
                if( mx >= x + ow - pad && mx <= x + ow + pad && my >= y1)
                {
                    $('html,body').css('cursor','e-resize');
                    nohover = false;
                    canDragColumn = i;
                }
            }

            // Mouse not hovering near column edges
            if( nohover == true )
            {
                $('html,body').css('cursor','auto');
                canDragColumn = -1;
            }
        });
    }
    else
    {
        // A column is being dragged -> handle dragging
        if (dragTick > dragTickFreq) {
            $(".tableLiItem, .tableLiHeader").each(function(index, element){
                let sp = $(this).find('span')[draggingColumn];
                $(sp).width(mx - $(sp).offset().left );
            });

            dragTick = 0;
        } else {
            dragTick++;
        }

    }
});

// Clicking will toggle column dragging
$(body).on("click", function(e) {

    if( canDragColumn >= 0 )
    {
        if( draggingColumn < 0 )
        { draggingColumn = canDragColumn; }
        else
        { draggingColumn = -1;
            dragTick = 0;
        }
    }
});

// Get building data and draw in a table
function drawBuildingData(buildid)
{
    let getData = GetAjaxCall('buildingData', { bid: buildid });
    getData.then((result) => {

        let headers = ['Sensorit', 'Huoneet', 'Hälytykset'];
        let tableids = ['sensors', 'rooms', 'alerts']
        let obkeys = Object.keys(result);
        let rset_sens = result.result_sensors;
        let rset_rooms = result.result_rooms;
        let rset_alerts = result.result_alerts;
        let table_width = 220;
        let cellclass = "btCell";
        let marg = 32;

        // Remove previous data if there is any
        $(".survBdata").remove();
        
        // Insert all headers in the first row
        let h;
        let ww = 0;
        for( let i = 0; i < headers.length; i++ )
        {
            h = headers[i];

            // Table itself
            let buildDiv = createElement('div', "", "survBdata");
            $(buildDiv).css({"overflow-y":"scroll", "margin-left": marg + ww + "px", "margin-top":"48px", "height":"240px"});
            appendTo(survBuildingData, buildDiv);
            let buildTable = createElement('table', 'buildTable' + tableids[i], 'survBdata');
            $(buildTable).css({"position":"relative"});
            appendTo(buildDiv, buildTable);

            // First row, the header
            let tr_header = createElement('tr', 'buildDheader' + i);
            appendTo(buildTable, tr_header);

            let th = createElement('th', 'btheader' + i);
            th.innerHTML = h;
            appendTo(tr_header, th);

            switch( i )
            {
                // Handle adding sensors to the table
                case 0:
                    for( let j in rset_sens )
                    {
                        let tr = createElement('tr', '0btrow' + j);
                        let td = createElement('td', '0btcell' + j, cellclass);
                        td.innerHTML = rset_sens[j].Sensor_ID;
                        td.style.cursor = "pointer";

                        appendTo(buildTable, tr);
                        appendTo(tr, td);
                    }
                    break;
                
                // Handle adding rooms to the table
                case 1:
                    for( let j in rset_rooms )
                    {
                        let tr = createElement('tr', '1btrow' + j);
                        let td = createElement('td', '1btcell' + j, cellclass);
                        td.innerHTML = rset_rooms[j].Room_name;
                        td.style.cursor = "pointer";

                        appendTo(buildTable, tr);
                        appendTo(tr, td);
                    }
                    break;
                
                // Handle adding alerts to the table
                case 2:
                    for( let j in rset_alerts )
                    {
                        let tr = createElement('tr', '2btrow' + j);
                        let d = new Date(rset_alerts[j].Sensor_alarm_date);
                        let dd = d.getDate();
                        let mm = d.getMonth() + 1;
                        let yy = d.getFullYear();
                        let hh = d.getHours();
                        let mn = d.getMinutes();
                        let ss = d.getSeconds();
                        let d_str = dd + "." + mm + "." + yy + "  " + hh + ":" + mn + ":" + ss;

                        // Alert description
                        let td = createElement('td', '21btcell' + j, cellclass);
                        td.innerHTML = rset_alerts[j].Sensor_alarm_description;
                        td.style.cursor = "pointer";

                        appendTo(buildTable, tr);
                        appendTo(tr, td);

                        // Alert date
                        td = createElement('td', '22btcell' + j);
                        td.innerHTML = d_str;
                        appendTo(tr, td);
                    }
                    break;
            }

            ww += +$(buildDiv).css("width").split("px")[0] + marg;
        }

        drawBuildingBlueprint(buildid);

        //appendTo(buildTable, createElement());
    }, (err) => {
        console.log('error!');
    });
}

// Draws the blueprint of a building
function drawBuildingBlueprint(buildid)
{
    $(".blueprint").remove();
    $(".roomBox").remove();

    let getData = GetAjaxCall('buildingBlueprint', { bID: buildid });
    getData.then((result) => {


        rooms = [];
        // Building surveillance styling: Table text
        let col_high = "rgb(255, 0, 0)";        // Highlight color
        let col_norm = "rgb(255, 255, 255)";    // Normal color

        // Blueprint styling: rooms
        let border_high = "1px solid #111";     // Border color when highlighted
        let border_norm = "1px dashed #111";    // Border color when normal
        let col_back = "#255";                  // Background color

        // Blueprint offset
        let bp_offset = {
            left: 400,
            top: 200
        }

        // Cycle through room result set to draw rooms
        let rset_rooms = result.rsetRooms;
        for( let i in rset_rooms )
        {
            let rid = rset_rooms[i].Room_ID;
            let rw = rset_rooms[i].Room_layout_width;
            let rh = rset_rooms[i].Room_layout_height;
            let ry = rset_rooms[i].Room_layout_y;
            let rx = rset_rooms[i].Room_layout_x;
            let rlid = rset_rooms[i].Room_layout_ID;
            
            // Add a room to the blueprint
            let bpr = createElement("div", "bp_room" + rid, "blueprint");
            $(bpr).css({
                "margin-left": rx - bp_offset.left,
                "margin-top": ry - bp_offset.top,
                "width": rw,
                "height": rh,
                "background": col_back,
                "border": border_norm,
                "position":"absolute"
            })

            let room = new Room(rooms.length, TYPE_ROOM, rid, rw, rh);
            room._X = rx;
            room._Y = ry;
            room._isNew = false;
            //rooms[rlid] = room;
            rooms.push(room);
            room._layoutID = rlid;

            console.log(room._index)
            createRoomElement(room._index);

            appendTo($("#survBlueprint"), bpr);

            // Upon click, highlight room and its sensors
            bpr.addEventListener('click', () => {

                // Change border to indicate selection
                $(".blueprint").css({"border": border_norm});
                $(bpr).css({"border": border_high});

                $(".btCell").each((index, element) => {

                    if( element.innerHTML == rset_rooms[i].Room_name )
                    { $(element).css({"color":col_high}) }
                    else
                    { $(element).css({"color":col_norm}) }
                });
            });

            // Link room in table to the blueprint
            $(".btCell").each((index, element) => {

                if( element.innerHTML == rset_rooms[i].Room_name )
                {
                    element.addEventListener('click', () => {

                        // Change border to indicate selection
                        $(".blueprint").css({"border": border_norm});
                        $(bpr).css({"border": border_high});

                        $(".btCell").css({"color":col_norm});
                        if( element.innerHTML == rset_rooms[i].Room_name )
                        { $(element).css({"color":col_high}) }
                    });
                }
            })

        }

        // Blueprint styling: sensors
        border_high = "2px solid orange";     // Border color when highlighted
        border_norm = "1px dashed #111";    // Border color when normal
        let sen_col_back = "tomato";                  // Background color

        // Cycle through sensor result set to draw sensors
        let rset_sensors = result.rsetSensors;
        for( let i in rset_sensors )
        {
            let rid = rset_sensors[i].Sensor_ID;
            let rx = rset_sensors[i].Sensor_layout_x;
            let ry = rset_sensors[i].Sensor_layout_y;
            let rlid = rset_sensors[i].Sensor_layout_ID;

            // Add a room to the blueprint
            let bpr = createElement("div", "bp_sensor" + rid, "blueprint");
            $(bpr).css({
                "margin-left": rx - bp_offset.left,
                "margin-top": ry - bp_offset.top,
                "width": sensorSize,
                "height": sensorSize,
                "background": sen_col_back,
                "border": border_norm,
                "position":"absolute"
            })

            let room = new Room(rooms.length, TYPE_SENSOR, rid, 0, 0);
            room._X = rx;
            room._Y = ry;
            room._isNew = false;
            //rooms[rlid] = room;
            rooms.push(room);
            room._layoutID = rlid;

            createRoomElement(room._index);

            appendTo($("#survBlueprint"), bpr);

            // Upon click, highlight room and its sensors
            bpr.addEventListener('click', () => {

                // Change border to indicate selection
                $(".blueprint").css({"border": border_norm});
                $(bpr).css({"border": border_high});

                $(".btCell").each((index, element) => {

                    if( element.innerHTML == rid )
                    { $(element).css({"color":col_high}) }
                    else
                    { $(element).css({"color":col_norm}) }
                });
            });

            // Link sensor in table to the blueprint
            $(".btCell").each((index, element) => {

                if( element.innerHTML == rset_sensors[i].Sensor_ID )
                {
                    element.addEventListener('click', () => {

                        // Change border to indicate selection
                        $(".blueprint").css({"border": border_norm});
                        $(bpr).css({"border": border_high});

                        $(".btCell").css({"color":col_norm});
                        if( element.innerHTML == rset_sensors[i].Sensor_ID )
                        { $(element).css({"color":col_high}) }
                    });
                }
            })
        }

        hideBlueprintElements(true);
    });
}

// Get property groups and buildings of given user 's'
function getUserData(s, _callback)
{
    let getData = GetAjaxCall('myData', { sID: s });
    getData.then((result) => {

        myBuildings = result;

        return _callback();
    });
}

// Sets the date range on Sensor data comparison to be CURRENT DATE - since
function setDateRangeSince(since)
{
    let d = new Date();
    d.setDate(d.getDate() - since);
    let dd1 = d.getDate();
    let mm1 = d.getMonth();
    let yy1 = d.getFullYear();

    dd1 = dateAddZero(dd1);
    mm1 = dateAddZero(mm1+1);


    $("#dateRangeStart").val(yy1 + "-" + mm1 + "-" + dd1);

    d = new Date();
    d.setDate(d.getDate());
    dd1 = d.getDate();
    mm1 = d.getMonth();
    yy1 = d.getFullYear();

    dd1 = dateAddZero(dd1);
    mm1 = dateAddZero(mm1+1);

    $("#dateRangeEnd").val(yy1 + "-" + mm1 + "-" + dd1);
}