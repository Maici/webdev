





// Ajax call GET
function GetAjaxCall(url, data) {
    return new Promise((resolve, reject) => {
        AjaxCall = $.ajax({
            type: 'GET',
            url: "http://localhost:3000/" + url,
            data: data,
            dataType: 'json',
            success: function (response) {
                return response;
            }
        });

        AjaxCall.then((result) => {
            if (result) {
                resolve(result);
            } else {
                reject('error!');
            }
        });
    });
}


// Ajax call POST
function POSTAjaxCall(url, data) {
    return new Promise((resolve, reject) => {
        AjaxCall = $.ajax({
            type: 'POST',
            url: "http://localhost:3000/" + url,
            data: data,
            dataType: 'json',
            success: function (response) {
                return response;          
            }
        });

        AjaxCall.then((result) => {
            if (result) {
                resolve(result);
            } else {
                reject('error!');
            }
        });
    });
}







