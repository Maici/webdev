CREATE DATABASE Hackathon;

USE Hackathon;

CREATE TABLE Property_group
(
  Property_group_ID INT NOT NULL,
  Property_group_name VARCHAR(225) NOT NULL,
  PRIMARY KEY (Property_group_ID)
);

CREATE TABLE Building
(
  Building_ID INT NOT NULL,
  Building_name VARCHAR(225) NOT NULL,
  Building_address VARCHAR(225),
  Building_description VARCHAR(225),
  Property_group_ID INT NOT NULL,
  PRIMARY KEY (Building_ID),
  FOREIGN KEY (Property_group_ID) REFERENCES Property_group(Property_group_ID)
);

CREATE TABLE Room
(
  Room_ID INT NOT NULL,
  Room_name VARCHAR(225) NOT NULL,
  Building_ID INT NOT NULL,
  PRIMARY KEY (Room_ID),
  FOREIGN KEY (Building_ID) REFERENCES Building(Building_ID)
);

CREATE TABLE Sensor_type
(
  Sensor_type_ID INT NOT NULL,
  Sensor_type_name VARCHAR (225),
  Sensor_type_model VARCHAR (225),
  Sensor_type_path VARCHAR(225),
  PRIMARY KEY (Sensor_type_ID)
);

CREATE TABLE Sensor
(
  Sensor_ID INT NOT NULL,
  Sensor_description VARCHAR(225),
  Building_ID INT NOT NULL,
  Room_ID INT NOT NULL,
  Sensor_type_ID INT NOT NULL,
  PRIMARY KEY (Sensor_ID),
  FOREIGN KEY (Building_ID) REFERENCES Building(Building_ID),
  FOREIGN KEY (Room_ID) REFERENCES Room(Room_ID),
  FOREIGN KEY (Sensor_type_ID) REFERENCES Sensor_type(Sensor_type_ID)
);

CREATE TABLE Sensor_value
(
  Sensor_value_ID INT NOT NULL,
  Sensor_value_atr DOUBLE (100, 3),
  Sensor_value_date DATETIME,
  Sensor_ID INT NOT NULL,
  PRIMARY KEY (Sensor_value_ID),
  FOREIGN KEY (Sensor_ID) REFERENCES Sensor(Sensor_ID)
);

CREATE TABLE Sensor_ref_value
(
  Sensor_ref_value_ID INT NOT NULL AUTO_INCREMENT,
  Sensor_ref_value_min DOUBLE (100, 3),
  Sensor_ref_value_max DOUBLE (100, 3),
  Sensor_ID INT NOT NULL,
  PRIMARY KEY (Sensor_ref_value_ID),
  FOREIGN KEY (Sensor_ID) REFERENCES Sensor(Sensor_ID)
);

CREATE TABLE Staff
(
  Staff_ID INT NOT NULL,
  Staff_first_name VARCHAR(225) NOT NULL,
  Staff_surname VARCHAR(225),
  Staff_job_description VARCHAR(225),
  PRIMARY KEY (Staff_ID)
);

CREATE TABLE Account
(
  Account_ID INT NOT NULL,
  Account_name VARCHAR(225) NOT NULL,
  Account_password VARCHAR(225) NOT NULL,
  Staff_ID INT NOT NULL,
  PRIMARY KEY (Account_ID),
  FOREIGN KEY (Staff_ID) REFERENCES Staff(Staff_ID)
);

CREATE TABLE works_at
(
  Property_group_ID INT NOT NULL,
  Staff_ID INT NOT NULL,
  FOREIGN KEY (Property_group_ID) REFERENCES Property_group(Property_group_ID),
  FOREIGN KEY (Staff_ID) REFERENCES Staff(Staff_ID)
);

CREATE TABLE Sensor_alarm
(
  Sensor_alarm_ID INT NOT NULL,
  Sensor_alarm_description VARCHAR(225),
  Sensor_alarm_date DATETIME,
  Sensor_value_ID INT NOT NULL,
  PRIMARY KEY (Sensor_alarm_ID),
  FOREIGN KEY (Sensor_value_ID) REFERENCES Sensor_value(Sensor_value_ID)
);

CREATE TABLE Sensor_analysis
(
  Sensor_analysis_ID INT NOT NULL,
  Sensor_analysis_name VARCHAR(225) NOT NULL,
  Sensor_analysis_description VARCHAR(225),
  Sensor_analysis_date DATE,
  Sensor_value_ID INT NOT NULL,
  PRIMARY KEY (Sensor_analysis_ID),
  FOREIGN KEY (Sensor_value_ID) REFERENCES Sensor_value(Sensor_value_ID)
);

CREATE TABLE Sensor_report
(
  Sensor_report_ID INT NOT NULL,
  Sensor_report_name VARCHAR(225) NOT NULL,
  Sensor_report_description VARCHAR(225),
  Sensor_report_date DATE,
  Sensor_value_ID INT NOT NULL,
  PRIMARY KEY (Sensor_report_ID),
  FOREIGN KEY (Sensor_value_ID) REFERENCES Sensor_value(Sensor_value_ID)
);

CREATE TABLE Room_layout
(
  Room_layout_ID INT NOT NULL,
  Room_layout_width DOUBLE (100, 3),
  Room_layout_height DOUBLE (100, 3),
  Room_layout_x DOUBLE (100, 3),
  Room_layout_y DOUBLE (100, 3),
  Room_ID INT NOT NULL,
  PRIMARY KEY (Room_layout_ID),
  FOREIGN KEY (Room_ID) REFERENCES Room(Room_ID)
);

CREATE TABLE Sensor_layout
(
  Sensor_layout_ID INT NOT NULL,
  Sensor_layout_x DOUBLE (100, 3),
  Sensor_layout_y DOUBLE (100, 3),
  Sensor_ID INT NOT NULL,
  PRIMARY KEY (Sensor_layout_ID),
  FOREIGN KEY (Sensor_ID) REFERENCES Sensor(Sensor_ID));