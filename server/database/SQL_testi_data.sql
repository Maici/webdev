INSERT INTO Property_group
(Property_group_ID, Property_group_name)
VALUES  (1, 'Niinivaara'),
        (2, 'Keskusta'),
        (3, 'Hukanhauta'),
        (4, 'Utra'),
        (5, 'Penttilä');

INSERT INTO Building
(Building_ID, Building_name, Building_address, Building_description, Property_group_ID)
VALUES  (1, 'Pisin kerrostalo olemassa', 'Kauhakuja 2', 'Uudenkarhea kerrostalo, joka haisee turpeelle', 5),
        (2, 'Vale-Verovirasto', 'Pimistystie 33', 'Homeessa. Remontoitu 1978', 4),
        (3, 'Keippilan päiväkoti', 'Keippilanmutka 2', 'Pirtsakka punainen maali pinnassa. Päiväkoti', 2),
        (4, 'A3-Varasto', 'Tekotehdastie 12', 'Varastorakennus. Remontoitu 2005', 3),
        (5, 'Karelia-ammattikorkeakoulu' , 'Karjalankatu 3' , 'Ammattiorkeakoulu', 5),
        (6, 'Nepenmäen koulu' , 'Kärpänkatu 7', 'Ala-aste', 2),
        (7, 'Penttilän Päiväkoti' , 'Vehkakuja 4' , 'Päiväkoti', 3),
        (8, 'Siun sote' , 'Tikkamäentie 16' , 'Julkinene terveyslaitos', 4),
        (9, 'Pohjois-Karjalan Osuuspankki Joensuun konttori', 'Koskikatu 9', 'Pankki' , 1);

INSERT INTO Room
(Room_ID, Room_name, Building_ID)
VALUES  (1, 'Huoneisto A2. Eteinen', 1),
        (2, 'Kellari', 2),
        (3, 'Kidutuskoppi', 3),
        (4, 'Varastohallin koillisnurkan wc', 4),
        (5, 'Luokka huone', 5),
        (6, 'Eteinen', 6),
        (7, 'Ruokailu tila', 7),
        (8, 'Lääkärin vastaanottohuone', 8);

INSERT INTO sensor_type
(Sensor_type_ID, Sensor_type_name, Sensor_type_model)
VALUES  (1, 'Murtohälytin', 'V1'),
        (2, 'Palohälytin', 'V1'),
        (3, 'Painesensori', 'MEGA-50'),
        (4, 'Hilavitkutin', 'TURBO+');

INSERT INTO Sensor
(Sensor_ID, Sensor_description, Building_ID, Room_ID, Sensor_type_ID)
VALUES  (1, 'Siirrettävä sensori', 3, 1, 1),
        (2, 'Kiinteä katonrajassa', 4, 2, 2),
        (3, 'Ilmastointiin asennettu', 6, 3, 3),
        (4, 'Katossa lymyilevä',5, 4, 4),
        (5, 'Oven yläpuolella', 2, 6, 1),
        (6, 'Katossa', 1, 7, 2),
        (7, 'Katossa', 3, 8, 2);

INSERT INTO Sensor_value
(Sensor_value_ID, Sensor_value_atr, Sensor_value_date, Sensor_ID)
VALUES  (1, 12.2, '1999-12-20 11:15:00', 1),
        (2, 9001.1, '2012-06-05 2:30:00', 2),
        (3, 78.4, '2011-11-25 6:10:00', 3),
        (4, 2.23, '2014-01-10 16:45:00', 4),
        (5, 7.5, '2019-11-21 12:00:00', 5),
        (6, 8.5, '2019-11-22 8:00:00', 6),
        (7, 9.2, '2019-11-23 23:00:00', 7),
		  (8, 14.5, '1999-12-23 1:15:00', 1),
		  (9, 8991.9, '2012-06-07 23:00:00', 2);
		  
INSERT INTO Sensor_ref_value
(Sensor_ref_value_ID, Sensor_ref_value_min, Sensor_ref_value_max, Sensor_ID)
VALUES  (1, 0, 100, 1),
        (2, -5000, 10000, 2),
        (3, 0, 50, 3),
        (4, 0, 10, 4),
        (5, 0, 10, 5),
        (6, 0, 30, 6),
        (7, 10, 100, 7),
		  (8, 0, 10, 1),
		  (9, 9000, 10000, 2);

INSERT INTO Staff
(Staff_ID, Staff_first_name, Staff_surname, Staff_job_description)
VALUES  (1, 'Repe', 'Kilihtinen', 'Liiman nuuhkija, siivooja ja salakuuntelija'),
        (2, 'Erkki-Diana', 'Mustonen', 'Alusvaatemalli ja monitoimihenkilö'),
        (3, 'Jarska', 'Kunikkala', 'Rehtori, vihtori ja tiedemies'),
        (4, 'Bubbles', '', 'Michael Jacksonin simpanssi'),
        (5, 'Raaka', 'Mirjami', 'Kurinpitäjä piiskuri'),
        (6, 'Anonyymi', 'Koodari', 'Admin'),
        (7,'Johan','Karppi','Työntekijä'),
        (8,'Jakki','Pakki','Työntekijä'),
        (9,'Kalmari','Kolmonen','Työntekijä'),
        (10,'Nappe','Lämäri','Työntekijä'),
        (11,'Kalevi','Kalmari','Työntekijä'),
        (12,'Pablo','Kasarmi','Työntekijä'),
        (13,'Hermanni','Kappeli','Työntekijä'),
        (14,'Gregorias','Jalli','Työntekijä');

INSERT INTO Account
(Account_ID, Account_name, Account_password, Staff_ID)
VALUES  (1, 'Testi' , 'QWERTY' , 1),
        (2, 'Kampele33' , 'murtuma21' , 2),
        (3, 'Karrikoira' , 'pamela7' , 3),
        (4, 'korppi1' , 'hassu123L' , 4),
        (5, 'naamrilöppi' , 'emtx2' , 5),
        (6, 'root', 'admin', 6);

INSERT INTO Sensor_alarm
(Sensor_alarm_ID, Sensor_alarm_description, Sensor_alarm_date, Sensor_value_ID)
VALUES  (1, 'Talo palaa ja kaikki on menetetty', '2000-11-11 4:15:00', 1),
        (2, 'Kylmä ku jäinen muikku', '2001-05-10 16:45:00', 2),
        (3, 'Hiilidioksidin määrä ilmassa hipoo huippua,', '1999-12-20 6:35:00', 3),
        (4, 'Murtohälyyttimen testaus', '2019-11-21 12:00:00', 4),
        (5, 'Palohälyyttimen testausa', '2019-11-23 10:00:00', 5),
        (6, 'Peruukin testaus', '2019-11-24 9:00:00', 6);

INSERT INTO Sensor_analysis
(Sensor_analysis_ID, Sensor_analysis_name, Sensor_analysis_description, Sensor_analysis_date, Sensor_value_ID)
VALUES  (1, 'Haistelu', 'Imastaan ilmaa sisään reippaasti molemmilla sieraimilla', '1999-12-20', 3),
        (2, 'Tuntuma', 'Kokeillaan kädellä onko lämmintä vai ei', '2002-02-20', 1),
        (3, 'Hipaisu', 'Nuolastaan metalli tolppaa pakkasella', '2009-10-07', 2),
        (4, 'Murtohälyyttimen sensori arvojen analyysi', 'Tässä on sensori arvojen analyysi tältä päivältä' , '2019-11-21', 4),
        (5, 'Palohälyyttimen sensori arvojen analyysi', 'Tässä on sensori arvojen analyysi tältä päivältä' , '2019-11-23', 5),
        (6, 'Palohälyyttimen sensori arvojen analyysi', 'Tässä on sensori arvojen analyysi tältä päivältä' , '2019-11-25', 6);
        
        INSERT INTO Sensor_report
(Sensor_report_ID, Sensor_report_name, Sensor_report_description, Sensor_report_date, Sensor_value_ID)
VALUES  (1, 'Suuri data raportti', 'Havaittiin suuria data arvoja', '2012-01-01', 3),
        (2, 'Data raportti', 'Haamu dataa pelottaa', '2015-02-04', 1);

INSERT INTO Room_layout
(Room_layout_ID, Room_layout_width, Room_layout_height, Room_layout_x, Room_layout_y, Room_ID)
VALUES  (1, 200, 300, 7.2, 3.2, 1),
        (2, 10 , 10 , 5 , 5 , 1),
        (3, 3 , 2 , 4 , 2 , 2);

INSERT INTO sensor_layout
(Sensor_layout_ID, Sensor_ID, Sensor_layout_x, Sensor_layout_y)
VALUES  (1, 1, 4, 5),
        (2, 2, 10, 2),
        (3, 3, 5, 6);

INSERT INTO works_at
(Property_group_ID, Staff_ID)
VALUES  (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (3, 6),
        (4, 7),
        (5, 8),
        (1, 9),
        (1, 11),
        (2, 12),
        (3, 13),
        (4, 14);