

# TO DO LIST


[BUGS]

- Saving building's blueprint will end to server crash, if there is either 0 rooms or sensors.

- Quotes missing from SQL statement
    - String values needs to be quoted


[SERVER]

- Code cleaning

- SQL statements needs rework

- user needs a confirmation when changing values to database, if it was succesful or not

- adding data to database from "Tietokannan hallinta" -tab doesn't work


[CLIENT]

- Code cleaning

- Add logic to new data functionality
    - Update "create new item form" to current table after clicking next
    - Fill IDs, if needed automatically (add new item)
        - Select building -> fill room view's 'add new item' buildingID

- Move search fields inside the filter panel above

- Add page for complete database management
    - Submenu for 'minor' database tables


- Filtering options needs work/ rework


- Table content's styling needs rework
    - disable overflow (hide)
        - hovering cells creates tooltip, if field's content is hidden

- Finalizing column resize function
    - add div for resize
        - => avoid sorting
    - resize mechanic
        - tick system?
        - "magnetic drag"?


# DONE / FIXED

- search field floats on top of other stuff on main site <-- not really done, but search field has been disabled for now
- profile icon "floats" on top of other stuff on main site
- Data management page "select all" checkbox is broken - Do Not Use
- Add missing database tables manually [body]
    - Make script for getting database table's column names
        - Find out if possible/ doable
- Add more testing data for each database table
- Update new database to server.js code [body]
- Add comments into CSS file
- Add 'edit' option for 'management' page
- Sensor comparison table glitching for some reason
- Add warnings data into randomDataGenerator
- New create element function
    - add parameters for id, className...
- column search field
- Action cancelling button in management page
    - while selecting sensors or building



# Trash bin

- Add dropdown menu for 'management page's 'add new record' fields
    - if cell is ID change it to name instead (use IDs after change)
        - do same in table content fields

- [1] Add cell size parameter for every field
    - small, medium, large

- [1] Use cell size parameters


