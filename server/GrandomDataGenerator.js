

// Include config file
const   conf = require('./config');


const   mysql = require('mysql');

console.log('generating data...');

wb = [
    ['miscreant', 'voiceless', 'angry', 'tremendous', 'nice', 'eatable', 'gainful', 'basic', 'fine', 'clear', 'nimble', 'uptight', 'greasy', 'grumpy', 'nondescript', 'lovely', 'disagreeable', 'actual', 'stormy', 'lucky', 'unsightly', 'noiseless', 'delicate', 'lewd', 'rhetorical', 'melted', 'shut', 'unwritten', 'rich', 'teeny-tiny', 'short', 'lyrical', 'pricey', 'round', 'jittery', 'eager', 'overrated', 'nasty', 'abnormal', 'zippy', 'icky', 'dependent', 'absorbed', 'husky', 'yellow', 'legal', 'useless', 'berserk', 'tired', 'inquisitive', 'massive', 'devilish', 'sweltering', 'lean', 'delirious', 'oceanic', 'picayune', 'best', 'jaded', 'truculent', 'kindly', 'auspicious', 'elated', 'heady', 'present', 'helpful', 'unused', 'billowy', 'wasteful', 'aloof', 'awesome', 'faded', 'ultra', 'wrathful', 'tiny', 'lacking', 'descriptive', 'fallacious', 'makeshift', 'telling', 'robust', 'tame', 'gabby', 'unusual', 'savory', 'literate', 'weak', 'difficult', 'thirsty', 'damp', 'able', 'hospitable', 'understood', 'incredible', 'green', 'puzzling', 'homeless', 'functional', 'one', 'impartial'],
    ['possess', 'revise', 'wrap', 'affect', 'multiply', 'inspire', 'abandon', 'reproduce', 'go', 'squeeze', 'account', 'succeed', 'protest', 'pretend', 'fancy', 'question', 'write', 'pursue', 'tip', 'rate', 'cut', 'capture', 'adapt', 'oppose', 'figure', 'sell', 'tremble', 'disturb', 'suck', 'dictate', 'burn', 'swallow', 'regulate', 'compare', 'emphasize', 'approve', 'benefit', 'eliminate', 'allocate', 'fear', 'couple', 'motivate', 'fulfil', 'progress', 'seek', 'tackle', 'assist', 'confuse', 'breathe', 'light', 'convince', 'express', 'form', 'switch', 'characterize', 'think', 'complain', 'divide', 'promote', 'organise', 'separate', 'touch', 'abolish', 'promise', 'demand', 'leap', 'characterise', 'refer', 'balance', 'catch', 'bring', 'use', 'top', 'kill', 'wonder', 'line', 'remark', 'injure', 'twist', 'fall', 'estimate', 'snap', 'glance', 'presume', 'deal', 'realise', 'relate', 'shift', 'tour', 'answer', 'flow', 'whisper', 'grip', 'occur', 'update', 'let', 'ignore', 'crash', 'smoke', 'outline'],
    ['information', 'presence', 'engineering', 'customer', 'income', 'apartment', 'law', 'piano', 'poetry', 'son', 'mood', 'actor', 'advice', 'energy', 'tale', 'measurement', 'perspective', 'area', 'poet', 'insurance', 'appointment', 'significance', 'leader', 'reading', 'requirement', 'power', 'courage', 'tradition', 'comparison', 'platform', 'tooth', 'guidance', 'emotion', 'committee', 'aspect', 'sector', 'world', 'philosophy', 'nation', 'winner', 'steak', 'suggestion', 'paper', 'pizza', 'interaction', 'flight', 'height', 'priority', 'basis', 'outcome', 'enthusiasm', 'connection', 'ear', 'entry', 'inspection', 'strategy', 'bonus', 'virus', 'personality', 'activity', 'competition', 'child', 'army', 'thought', 'ad', 'discussion', 'session', 'candidate', 'unit', 'depression', 'historian', 'response', 'administration', 'town', 'guest', 'cheek', 'psychology', 'bird', 'gene', 'salad', 'device', 'pollution', 'initiative', 'finding', 'advertising', 'analyst', 'physics', 'oven', 'football', 'celebration', 'phone', 'impression', 'computer', 'owner', 'song', 'decision', 'climate', 'intention', 'complaint', 'appearance'],
    ['lie', 'reptile', 'retiree', 'reveal', 'accident', 'crackpot', 'commemorate', 'money', 'policy', 'habitat', 'accept', 'nomination', 'biology', 'free', 'strategic', 'jacket', 'collect', 'genetic', 'marketing', 'swing', 'bake', 'hair', 'medieval', 'far', 'habit', 'premature', 'budget', 'name', 'funny', 'nationalist', 'entitlement', 'fireplace', 'peace', 'witch', 'update', 'apple', 'miner', 'agreement', 'dish', 'revival', 'bathtub', 'assumption', 'divide', 'fuss', 'belly', 'able', 'east', 'maid', 'brother', 'low', 'damn', 'stunning', 'equation', 'scene', 'overwhelm', 'index', 'exchange', 'feather', 'lot', 'berry', 'activity', 'conscious', 'bowel', 'leaflet', 'gossip', 'play', 'blind', 'injection', 'tail', 'sink', 'harmony', 'infinite', 'elbow', 'mud', 'giant', 'headline', 'veil', 'mind', 'pride', 'assault', 'confine', 'visual', 'highway', 'fragrant', 'treatment', 'athlete', 'balance', 'drum', 'perform', 'country', 'Bible', 'limited', 'memory', 'flock', 'revive', 'heart', 'calorie', 'maximum', 'west', 'banish']
];
// adj
// verb
// noun
// word


const conn = mysql.createConnection({
    host: 'localhost',
    user: conf.user,
    password: conf.password,
    port: 3306,
    database: 'hackathon'
});


magicNumber = 1;

// Create random int within range for words
function rand() {
    int = Math.round(Math.random() * 99 - 1);
    return int;
}
function bRand() {
    int = Math.round(Math.random() * 99999 - 1) * magicNumber;
    return int;
}
function mRand() {
    int = Math.round(Math.random() * 15 + 5) * magicNumber;
    return int;
}
function sRand() {
    int = Math.round(Math.random() * 5 + 2) * magicNumber;
    return int;
}

// Create random word
function randdor(words) {
    let l = words.length;
    let i = 0;
    let word = '';

    while (i != l) {
        if (i == 0) {
            word += wb[words[i]][rand()];
        } else {
            word += ' '+wb[words[i]][rand()];
        }
        i++;
    }
    return word;
}

execSQL("INSERT INTO staff(Staff_ID, Staff_first_name, Staff_surname, Staff_job_description) VALUES(1, 'Repe', 'Kilihtinen', 'Liiman nuuhkija, siivooja ja salakuuntelija');");

worksAtSet = false;



s = ['lampomittari','vedenkulutus','sahkonkulutus','paineC02']
sensorsN = 3;       // length - 1;

propertyN       = 2;
buildingN       = 6;
roomN           = 3;
sensorN         = 4;        // sensors per room
sensorDataN     = 200;   // sensordata per sensor
sensorAlertRate = 1/1000; // likelyhood of an alarm per sensor value


function doLoop() {

    let sensorDataID = 1;
    let sensorIDAI = 1; // AI = AUTO INCREMENT
    let sensorAlarmID = 1;

    for (a0=0; a0<s.length; a0++) {
        Sensor_Type_ID      = a0;
        Sensor_Type_Name    = s[a0];
        Sensor_Type_Model   = '4';

        sql = `INSERT INTO sensor_type VALUES ("${Sensor_Type_ID}", "${Sensor_Type_Name}", "${Sensor_Type_Model}", "null")`
        execSQL(sql);

    }

    for (a1=0; a1<propertyN; a1++) {
        Property_Group_ID       = bRand();
        Property_Group_Name     = randdor([1,0,2]);

        sql = `INSERT INTO property_group VALUES ("${Property_Group_ID}", "${Property_Group_Name}")`
        execSQL(sql);

        if( worksAtSet == false )
        {
            execSQL("INSERT INTO works_at(Property_group_ID, Staff_ID) VALUES (" + Property_Group_ID + ", 1);");
            worksAtSet = true;
        }


        for (a2=0; a2<buildingN; a2++) {
            Building_ID             = bRand();
            Building_Name           = randdor([1,3]);
            Building_Address        = randdor([0,2]) + ' ' + mRand();
            Building_Desc           = 'tyhja'
            Building_Property_ID    = Property_Group_ID;

            sql = `INSERT INTO building VALUES ("${Building_ID}","${Building_Name}","${Building_Address}","${Building_Desc}","${Building_Property_ID}")`
            execSQL(sql);

            for (a3=0; a3<roomN; a3++) {
                Room_ID             = Math.round(bRand() * Math.random());
                Room_Name           = randdor([0, 1, 3]);
                Room_Building_ID    = Building_ID

                sql = `INSERT INTO room VALUES ("${Room_ID}","${Room_Name}","${Room_Building_ID}")`
                execSQL(sql);

                for (a4=0; a4<sensorN; a4++) {
                    Sensor_ID           = sensorIDAI++;
                    Sensor_Desc         = 'tyhja'
                    Sensor_Building_ID  = Building_ID
                    Sensor_Room_ID      = Room_ID
                    Sensor_Type_ID      = Math.round(Math.random() * sensorsN);

                    sql = `INSERT INTO sensor VALUES ("${Sensor_ID}","${Sensor_Desc}","${Sensor_Building_ID}","${Sensor_Room_ID}","${Sensor_Type_ID}")`
                    execSQL(sql);

                        Sensor_ID           = Sensor_ID
                        startValue          = 21;
                        start_date          = 1574690324;
                        tick                = 86400;

                        for (a6=0; a6<sensorDataN; a6++) {
                            Sensor_value_ID     = sensorDataID++;
                            Sensor_value_atr    = startValue += (Math.round(Math.random() * 2 - 1) * 100) / 100;

                            currentTime_date = Math.round((start_date - (tick * a6)) * 1000);
                            Sensor_value_date   = new Date(currentTime_date);

                            let date_ob = Sensor_value_date;

                            let date = ("0" + date_ob.getDate()).slice(-2);
                            let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
                            let year = date_ob.getFullYear();
                            let hours = date_ob.getHours();
                            let minutes = date_ob.getMinutes();
                            let seconds = date_ob.getSeconds();
                        
                            let dateTime = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;

    
                            sql = `INSERT INTO sensor_value VALUES ("${Sensor_value_ID}","${Sensor_value_atr}","${dateTime}","${Sensor_ID}")`;
                            execSQL(sql);

                            // Generate alarms according to alert likelyhood
                            if( Math.random() <= sensorAlertRate )
                            {
                                let alert;
                                let Sensor_alarm_ID = sensorAlarmID++;
                                let Sensor_alarm_desc = randdor([0, 3, 2, 1]);
                                sql = `INSERT INTO sensor_alarm VALUES ("${Sensor_alarm_ID}","${Sensor_alarm_desc}","${dateTime}","${Sensor_value_ID}")`;
                                execSQL(sql);
                            }

                        
                    }
                }
            }
        }
    }
    console.log('Data generated!');
    console.log('Closing generator...');
    setTimeout(() => {
        conn.end();
    }, 2000);
}





doLoop();

function execSQL(sql) {
    conn.query(sql, (err) => {
        if (err) {
            console.log(err);
        }

    });

}




console.log('piip piip...');

    



