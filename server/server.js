console.log('Starting server...');

// Include config file
const   conf = require('./config');

const   express = require('express'),
        mysql = require('mysql'),
        bodyParser = require('body-parser'),
        cors = require('cors');

const   app = express();

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

const serverConf =
{
    host: 'localhost',
    port: 3000
};

const conn = mysql.createConnection({
    host: 'localhost',
    user: conf.user,
    password: conf.password,
    port: 3306,
    database: 'hackathon'
});

app.listen(serverConf);

// Server routing
app.get('/', (req, res) => {
    console.log('serving files');
    res.sendFile(__dirname + '/public/index.html');
});

app.get('/home', (req, res) => {
    console.log('serving files');
    res.sendFile(__dirname + '/public/index.html');
});

// Get resultset from database
app.get('/records', (req, res) => {
    let table = req.query.type;
    let ids = req.query.selectedItem;

	const tableKeys =
	{
		"sensor": "Room_ID",
		"building": "Property_group_ID",
		"room": "Building_ID",
		"property_group": "Property_group_ID",
		"sensor_value": "Sensor_value_ID"
	}
	
	const tableBodyFormats =
	{
		"account":
			[['Account ID', 'Account name', 'Account password', 'Staff ID'],
			['Account_ID', 'Account_name', 'Account_password', 'Staff_ID'],
			[true, true, true]],
		"building":
			[['Building ID', 'Name', 'Address', 'Desc', 'Property_group_ID'],
			['Building_ID', 'Building_name', 'Building_address', 'Building_description', 'Property_group_ID'],
			[true, true, true, true, true]],
		"building_property_group":
			[['Building PG ID', 'Building ID', 'Property group ID'],
			['Building_property_group_ID', 'Building_ID', 'Property_group_ID'],
			[true, true, true]],
		"property_group":
			[['Property Group ID', 'Property Group Name'],
			['Property_group_ID', 'Property_group_name'],
			[true, true]],
		"room":
			[['Room ID', 'Room name', 'Building ID'],
			['Room_ID', 'Room_name', 'Building_ID'],
			[true, true, true]],
		"room_layout":
			[['Room layout ID', 'Room layout width', 'Room layout height', 'Room layout X', 'Room layout Y', 'Room ID'],
			['Room_layout_ID', 'Room_layout_width', 'Room_layout_height', 'Room_layout_X', 'Room_layout_Y', 'Room_ID'],
			[true, true, true, true, true, true]],
		"sensor":
			[['Sensor ID', 'Sensor desc', 'Building ID', 'Room ID', 'Sensor type ID'],
			['Sensor_ID', 'Sensor_description', 'Building_ID', 'Room_ID', 'Sensor_type_ID'],
			[true, true, true, true, true]],
		"sensor_alarm":
			[['Sensor alarm ID', 'Sensor alarm desc', 'Sensor alarm date', 'Sensor value ID'],
			['Sensor_alarm_ID', 'Sensor_alarm_description', 'Sensor_alarm_date', 'Sensor_value_ID'],
			[true, true, true, true]],
		"sensor_analysis":
			[['Sensor analysis ID', 'Sensor analysis name', 'Sensor analysis desc', 'Sensor analysis date', 'Sensor value ID'],
			['Sensor_analysis_ID', 'Sensor_analysis_name', 'Sensor_analysis_description', 'Sensor_analysis_date', 'Sensor_value_ID'],
			[true, true, true, true, true]],
		"sensor_layout":
			[['Sensor layout ID', 'Sensor layout path', 'Sensor ID'],
			['Sensor_layout_ID', 'Sensor_layout_path', 'Sensor_ID'],
			[true, true, true]],
		"sensor_report":
			[['Sensor report ID', 'Sensor report name', 'Sensor report desc', 'Sensor report date', 'Sensor value ID'],
			['Sensor_report_ID', 'Sensor_report_name', 'Sensor_report_description', 'Sensor_report_value', 'Sensor_value_ID'],
			[true, true, true, true, true]],
		"sensor_type":
			[['Sensor type ID', 'Sensor type name', 'Sensor type model'],
			['Sensor_type_ID', 'Sensor_type_name', 'Sensor_type_model'],
			[true, true, true]],
		"sensor_value":
			[['Sensor value ID', 'Sensor value atr', 'Sensor value date', 'Sensor ID'],
			['Sensor_value_ID', 'Sensor_value_atr', 'Sensor_value_date', 'Sensor_ID'],
			[true, true, true, true]],
		"staff":
			[['Staff ID', 'First name', 'Surname', 'Job description'],
			['Staff_ID', 'Staff_first_name', 'Staff_surname', 'Staff_job_description'],
			[true, true, true, true]],
		"works_at":
			[['Building ID', 'Staff ID'],
			['Building_ID', 'Staff_ID'],
			[true, true]]
	}
	
	if (!table)
	{
        res.sendStatus(404);
		return;
	}

    // Filtering parameters checking here!
    // In switch section add filtering options into sql statement

    let sql = 'SELECT * FROM ' + table;

    // Format for every result set. Sensors, buildings, people, etc.
    let body =
	{
        headers: [],
        keys: [],
        visible: [],
        resultSet: []
    }
	
	let bodyFormat = tableBodyFormats[table];
	
	body.headers = bodyFormat[0];
	body.keys = bodyFormat[1];
	body.visible = bodyFormat[2];

    let tableKey = '';
    let inArray = '(';
    
	for (let i in ids)
	{
        if (i != ids.length - 1)
		{
            inArray += ids[i] + ',';
        }
		else
		{
            inArray += ids[i]
        }
    }
        
	inArray += ')';

    if (ids)
	{
        tableKey = tableKeys[table];
        sql += ' WHERE '+tableKey+' IN ' + inArray;
    }
/*
    if (table == 'building' && ids) {
            sql = `SELECT * FROM building AS a
            INNER JOIN building_property_group AS b ON b.Building_ID=a.Building_ID
            INNER JOIN property_group AS c ON b.Property_group_ID=c.Property_group_ID WHERE
            c.property_group_ID IN ${inArray}`
        }
*/

    // Include alert location data
    if( table == 'sensor_alarm' )
    {
        sql = "SELECT pg.Property_group_name, b.Building_ID, b.Building_address, sa.Sensor_alarm_description, sa.Sensor_alarm_date" + 
        " FROM property_group pg, building b, sensor s, sensor_value sv, sensor_alarm sa" +
        " WHERE pg.Property_group_ID = b.Property_group_ID AND" + 
        " b.Building_ID = s.Building_ID AND" + 
        " s.Sensor_ID = sv.Sensor_ID AND" + 
        " sv.Sensor_value_ID = sa.Sensor_value_ID" + 
        " GROUP BY sa.Sensor_value_ID;";
    }

    // DEBUG
    console.log(sql);

    conn.query(sql, (err, resultSet) =>
	{
        body.resultSet = resultSet;
        res.json(body);
    });
});

// Login
app.get('/login', (req, res) => {
    let sql = "SELECT * FROM ACCOUNT WHERE Account_name = '" + req.query.uname + "' AND Account_password = '" + req.query.upass +"';";

    // Perform query
    conn.query(sql, (err, resultSet) => {

        // If an account with given username and password exist
        if( resultSet.length > 0 )
        { res.jsonp({result: "Login successful!"}); }
        else
        { res.jsonp({result: "Login failed!"}); }

    });
});

// Add new item to database
// WORKS ONLY WITH OLD DATABASE!!!
// Update database table's column names above first
app.post('/addRecord', (req, res) => {
    //let arr = Math.round(Math.random() * 1000) + ',';
	let arr = "";

    let l = req.body.fields.length;

    for (let i = 0; i < l; i++)
	{
        if (i != l-1)
		{
            arr += '"'+req.body.fields[i]+'",';
        }
		else
		{
            arr += '"'+req.body.fields[i]+'"';
        }
    }

    sql = 'INSERT INTO ' + req.body.table + ' VALUES ('
    sql += arr;
    sql += ')';

    conn.query(sql, (err, result) =>
	{
        if (err)
		{
            console.log(err)
            res.json({result:"fail"});
        }
        res.json(result);
    });
});


// Add sensor data
app.post('/sensor/:id', (req, res) => {
    let date_ob = new Date();

    let date = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    let year = date_ob.getFullYear();
    let hours = date_ob.getHours();
    let minutes = date_ob.getMinutes();
    let seconds = date_ob.getSeconds();

    let dateTime = year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;

    sensor_value_ID     = req.params.id + Math.round(Math.random() * 10000);
    sensor_Value_atr    = req.body.value;
    sensor_value_date   = dateTime;
    sensor_ID           = req.params.id;

    let sql = `INSERT INTO sensor_value VALUES ("${sensor_value_ID}","${sensor_Value_atr}","${sensor_value_date}","${sensor_ID}")`;

    conn.query(sql, (err, result) => {
        if (err) {
            res.json(0);
        } else {
            res.json(1);
        }
    });

});


// Update an entry in database
app.post('/modRecord', (req, res) => {
    
    let k = Object.keys(req.body);
    let id = req.body[k[1]];

    // Table being updated
    let sql = "UPDATE " + req.body.table + " SET ";
    
    // Update properties
    let l = k.length;
    for( let i = 2; i < l; i ++ )
    {
        sql += k[i] + " = '" + req.body[k[i]]

        if( l > i + 1 )
        { sql += "', "; }
        else
        { sql += "' "; }
    }
    
    // ID of the entry being updated
    sql += 'WHERE ' + k[1] + " = " + id;


    // Perform query
    conn.query(sql, (err, result) => {
        if (err) {
            console.log(err)
            res.json({result:"fail"});
        }
        res.json(result);
    });

});

// Add custom alert
app.post('/addCustomAlert', (req, res) => {

    // Prepare sql
    let sql = 'INSERT INTO Sensor_ref_value(Sensor_ref_value_min, Sensor_ref_value_max, Sensor_ID) VALUES (';
    let sid = req.body.sensID;
    let vmin = req.body.valueMin;
    let vmax = req.body.valueMax;

    sql += vmin + ', ' + vmax + ', ' + sid + ')';

    // Add new alert values to reference values
    conn.query(sql, (err, result) => {
        if (err) {
            console.log(err)
            res.json({result:"fail"});
        }
        res.json(result);
    });
});

// Get building data from database
app.get('/buildingData', (req, res) => {

    let buildid = req.query.bid;
    let rset_sens = {};
    let rset_rooms;
    let rset_alerts;
    let sql;

    // Get sensors of the building
    sql = "SELECT Sensor_ID FROM sensor WHERE Building_ID = " + buildid;
    conn.query(sql, (err, result) => {
        if (err) {
            console.log(err)
        }

        rset_sens = result;

        // Get rooms of the building
        sql = "SELECT Room_name FROM room WHERE Building_ID = " + buildid;
        conn.query(sql, (err, result) => {
            if (err) {
                console.log(err)
            }
            rset_rooms = result;

            let pre_sql;
            pre_sql = "SELECT sa.Sensor_alarm_description, sa.Sensor_alarm_date" + 
            " FROM property_group pg, building b, sensor s, sensor_value sv, sensor_alarm sa" +
            " WHERE pg.Property_group_ID = b.Property_group_ID AND" + 
            " b.Building_ID = s.Building_ID AND" + 
            " s.Sensor_ID = sv.Sensor_ID AND" + 
            " sv.Sensor_value_ID = sa.Sensor_value_ID AND" + 
            " sv.Sensor_ID IN (";

            sql = pre_sql;

            let vals = [];

            // Set IDs of the sensors whose alerts we want
            for( let i in rset_sens )
            {
                sql += rset_sens[i].Sensor_ID;

                if( i < rset_sens.length - 1 )
                { sql += ", "; }
            }

            if( sql == pre_sql )
            { sql += "-1"; }
            sql += ") GROUP BY sa.Sensor_value_ID;";
            console.log(sql);

            // Finally, get alerts
            conn.query(sql, (err, result) => {
                if (err) {
                    console.log(err)
                }
                
                rset_alerts = result;

                res.json({result_sensors: rset_sens, result_rooms: rset_rooms, result_alerts: rset_alerts});
            });
        });
    });
});

// Get buildings by staff id
app.get('/myData', (req, res) => {

    let staffid = req.query.sID;
    let sql = "SELECT b.Property_group_ID, b.Building_ID, b.Building_name, p.Property_group_name " + 
              "FROM building b, works_at wa, property_group p " + 
              "WHERE b.Property_group_ID = wa.Property_group_ID AND " +
              "p.Property_group_ID = b.Property_group_ID AND " +
              "wa.Staff_ID = " + staffid;
            
              console.log(sql)
    // Query
    conn.query(sql, (err, result) => {
        if (err) {
            console.log(err)
        }

        res.json(result);
    });
});

// Get sensor analyses by building id
app.get('/buildingAnalyses', (req, res) => {

    let propids = req.query.propertyGroups;
    let propids_len = propids.length;
    let propstr = "";

    // Get IDs of all the property groups
    for( let i in propids )
    {
        if( i < propids_len - 1 )
        { propstr += propids[i].Property_group_ID + ", "; }
        else
        { propstr += propids[i].Property_group_ID; }
    }

    let sql = "SELECT san.Sensor_analysis_ID, san.Sensor_analysis_name" +
              " FROM building b, sensor s, sensor_value sv, sensor_analysis san" +
              " WHERE b.Property_group_ID IN (" + propstr + ") AND" +
              " s.Building_ID = b.Building_ID AND" +
              " sv.Sensor_ID = s.Sensor_ID AND" +
              " san.Sensor_value_ID = sv.Sensor_value_ID;";
            
              console.log(sql)
    // Query
    conn.query(sql, (err, result) => {
        if (err) {
            console.log(err)
        }

        res.json(result);
    });
});

// Get blueprint data by building ID
app.get('/buildingBlueprint', (req, res) => {

    let buildid = req.query.bID;
    let sql = "SELECT rl.Room_ID, r.Room_name, rl.Room_layout_x, rl.Room_layout_y, rl.Room_layout_width, rl.Room_layout_height, rl.Room_layout_ID" +
              " FROM room r, Room_layout rl" +
              " WHERE r.Room_ID = rl.Room_ID AND" +
              " r.Building_ID = " + buildid;
    
    let res_rooms, res_sensors;
            
    console.log(sql)

    // First get rooms
    conn.query(sql, (err, result) => {
        if (err) {
            console.log(err)
        }

        res_rooms = result;

        // Then get sensors
        sql = "SELECT sl.Sensor_ID, sl.Sensor_layout_x, sl.Sensor_layout_y, sl.Sensor_layout_ID" +
              " FROM sensor s, Sensor_layout sl" +
              " WHERE s.Sensor_ID = sl.Sensor_ID AND" +
              " s.Building_ID = " + buildid;

        conn.query(sql, (err, result) => {
            if (err) {
                console.log(err)
            }
    
            res_sensors = result;
            res.json({ rsetRooms: res_rooms, rsetSensors: res_sensors });
        });
    });
});

// Edit a blueprint or add a new one if it doesn't exit
app.post('/updateBlueprint', (req, res) => {

    let sql = "SELECT MAX(Room_layout_ID) AS lastid" + 
              " FROM room_layout" + 
              " UNION ALL" +
              " SELECT MAX(Sensor_layout_ID) AS lastid" +
              " FROM sensor_layout;";

    // First get the largest room layout ID
    conn.query(sql, (err, result) => {
        if (err) {
            console.log(err)
            }
            
        let lastid = +result[0].lastid;
        let lastids = +result[1].lastid;
        let r_data = req.body.roomData;
        let s_data = req.body.sensorData;
        let edit = req.body.editing;
        let return_rset = [];

        // No room data provided, cannot save a blueprint
        if( r_data === undefined )
        { res.json({result:"fail"});return; }

        if( lastid === undefined )
        { lastid = 0; }

        if( lastids === undefined )
        { lastids = 0; }

        // Cycle through room data
        let r, rid, lid;

        // Only generate the query, if room data was sent
        sql = "INSERT INTO Room_layout (Room_layout_ID, Room_layout_width, Room_layout_height, Room_layout_x, Room_layout_y, Room_ID)" +
                " VALUES ";

        let r_data_len = r_data.length;
        for( let i in r_data )
        {
            r = r_data[i];

            // Check if the room has been added
            if( r.isNew == "true" )
            {
                console.log(r.isNew)
                lid = lastid + +i + 1;
                return_rset.push({eid: r.rID, lid: lid});
            }
            else
            { lid = r.layID; }

            // Add to query
            subsql = "(" + lid + ", " + r.rWidth + ", " + r.rHeight + ", " + r.rX + ", " + r.rY + ", " + r.rID + ")";

            if( i < r_data_len - 1 )
            { sql += subsql + ", "; }
            else
            { sql += subsql; }
        }

        // If data already exists, modify it
        sql += " ON DUPLICATE KEY UPDATE" +
            " Room_layout_width = VALUES(Room_layout_width)," +
            " Room_layout_height = VALUES(Room_layout_height)," +
            " Room_layout_x = VALUES(Room_layout_x)," +
            " Room_layout_y = VALUES(Room_layout_y)," +
            " Room_ID = VALUES(Room_ID);";
        

        let sql2 = "";

        // Only generate the query, if sensor data was sent
        if( s_data != undefined )
        {
            sql2 = " INSERT INTO Sensor_layout (Sensor_layout_ID, Sensor_layout_x, Sensor_layout_y, Sensor_ID) VALUES ";

            // Cycle through sensor data
            let s_data_len = s_data.length;
            for( let i in s_data )
            {
                r = s_data[i];

                // Check if the sensor has been added
                if( r.isNew == "true" )
                {
                    lid = lastids + +i + 1;
                    return_rset.push({eid: r.rID, lid: lid});
                }
                else
                { lid = r.layID; }

                // Add to query
                subsql = "(" + lid + ", " + r.rX + ", " + r.rY + ", " +  r.rID + ")";

                if( i < s_data_len - 1 )
                { sql2 += subsql + ", "; }
                else
                { sql2 += subsql; }
            }

            // If data already exists modify it
            sql2 += " ON DUPLICATE KEY UPDATE" +
                    " Sensor_layout_x = VALUES(Sensor_layout_x)," +
                    " Sensor_layout_y = VALUES(Sensor_layout_y)," +
                    " Sensor_ID = VALUES(Sensor_ID);";
            
        }

            // Query
        conn.query(sql, (err, result) => {
            if (err) {
                console.log(err)
                res.json({result: "fail"});
            }

            // Query
            if( s_data != undefined )
            {
                conn.query(sql2, (err, result) => {
                    if (err) {
                        console.log(err)
                        res.json({result: "fail"});
                    }
        
                    if( return_rset.length > 0 )
                    { res.json(return_rset); }
                    else
                    { res.json({result:"success"}); }
                });
            }
            else
            {
                if( return_rset.length > 0 )
                { res.json(return_rset); }
                else
                { res.json({result:"success"}); }
            }

        });
    });
});

app.post('/deleteFromBlueprint', (req, res) => {

    let etype = req.body.elmType;
    let eid = req.body.elmID;

    // No/invalid ID -> exit
    if( eid == undefined || eid == "" )
    { return; }

    let sql = "";

    switch( etype )
    {
        // Removing a room
        case "1":
            sql = "DELETE FROM room_layout WHERE Room_layout_ID = " + eid;
            break;
        
        // Removing a sensor
        case "2":
            sql = "DELETE FROM sensor_layout WHERE Sensor_layout_ID = " + eid;
            break;
    }

    // Non-existing element type (room/sensor) -> exit
    if( sql == "" )
    { return; }

    // First get the largest room layout ID
    conn.query(sql, (err, result) => {
        if (err) {
            console.log(err)
            res.json({result:"fail"});
            }
        
            res.json({result:"success"});
    });
});