

// Include modules
let mysql = require('mysql');
let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let morgan = require('morgan');

// Start webserver
app.listen(3000);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.use(express.static('public'));

app.use(morgan('combined'))

// SQL server connection configuration
let conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'db'
});

// Create connection to SQL server
conn.connect(function(err)
{
    if(err) throw err;
    console.log('Database connection!');
});



// Webserver routing
app.get('/', function(req, res)
{
    res.sendFile(__dirname + '/index.html');
});

// Select from database

app.get('/reg/:id', function(req, res)
{
    let id = req.params.id;

    if (id == '') return res.json(404);

    let sql = `SELECT * FROM registration WHERE id = ${id}`;

    conn.query(sql, function(err, result)
    {
        if (err) throw err;
        console.log(result);
        res.json(result);        
    });

});



// Select every user from database
app.get('/regs', function(req, res)
{
    conn.query('SELECT * FROM registration', function(err, result)
    {
        if (err) throw err;
            res.json(result);
    });
});
// Add to database
app.post('/addreg', function(req, res)
{
    let person = req.body;
    let sql = 'INSERT INTO registration SET ?';
    conn.query(sql, person, function(err, result)
    {
        if (err) throw err;
        console.log(result);
        res.json(result);        
    });
});

// Update from database
app.put('/updatereg/:id', function(req, res)
{
    let id = req.body.id;
    let first = req.body.first;
    let last = req.body.last;
    let age = req.body.age;

    if (id == '') return res.json(404);

    let sql = `UPDATE registration SET  first = '${first}', last = '${last}', age = '${age}' WHERE id = ${id}`;

    conn.query(sql, function(err, result)
    {
        if (err) throw err;
        console.log(result);
        res.json(result);        
    });
});

// Delete from database
app.delete('/deletereg/:id', function(req, res)
{
    let id = req.body.id;

    if (id == '') return res.json(404);


    let sql = `DELETE FROM registration WHERE id = ${id}`;

    conn.query(sql, function(err, result)
    {
        if (err) throw err;
        console.log(result);
        res.json(result);        
    });
});














