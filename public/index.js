




$('#box_submit_button').click(function(e)
{
    action = $('#box_action').val();
    id = $('#box_id').val();
    first = $('#box_first').val();
    last = $('#box_last').val();
    age = $('#box_age').val();

    AjaxCall(id, action, first, last, age);
});


function AjaxCall(id, action, first, last, age)
{
    if (action == 'select_all')
    {
        type = 'get';
        url = 'regs';
        data = {};
    };

    if (action == 'select')
    {
        type = 'get';
        url = `reg/${id}`;
        data = {}
    };

    if (action == 'add')
    {
        type = 'post';
        url = 'addreg';
        data = {first:first, last:last, age:age};
    };

    if (action == 'update')
    {
        type = 'put';
        url = 'updatereg/:id='+id;
        data = {first:first, last:last, age:age, id:id}
    };

    if (action == 'delete')
    {
        type = 'delete';
        url = 'deletereg/:id='+id;
        data = {first:first, id:id}
    };

    $.ajax({
        type: type,
        url: url,
        data: data,
        dataType: "json",
        success: function (response) {
            
            console.log(response);

        }
    });

}

